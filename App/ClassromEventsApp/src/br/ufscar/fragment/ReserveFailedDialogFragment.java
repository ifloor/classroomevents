package br.ufscar.fragment;

import br.ufscar.classromeventsapp.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class ReserveFailedDialogFragment extends DialogFragment {

	public static ReserveFailedDialogFragment newInstance(String s) {
		ReserveFailedDialogFragment f = new ReserveFailedDialogFragment();

		Bundle args = new Bundle();
		args.putCharArray("message", s.toCharArray());
		f.setArguments(args);

		f.setArguments(args);

		return f;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String message = new String(getArguments().getCharArray("message"));

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setMessage(message).setTitle(R.string.reserveFailed);
		
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
           public void onClick(DialogInterface dialog, int id) {
        	   ReserveFailedDialogFragment.this.getDialog().dismiss();
           }
       });

		return builder.create();
	}
}
