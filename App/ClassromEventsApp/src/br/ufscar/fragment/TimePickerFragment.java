package br.ufscar.fragment;

import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment implements
		TimePickerDialog.OnTimeSetListener {

	public interface TimePickerListener {
		public void setTime(int hourOfDay, int minute);
	}
	
	public static TimePickerFragment newInstance(Calendar c) {
		TimePickerFragment f = new TimePickerFragment();

		Bundle args = new Bundle();
		args.putInt("hour", c.get(Calendar.HOUR_OF_DAY));
		args.putInt("minute", c.get(Calendar.MINUTE));
		f.setArguments(args);

		return f;
	}
	
	private TimePickerListener mListener;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		int hour = getArguments().getInt("hour");
		int minute = getArguments().getInt("minute");

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hour, minute,
				DateFormat.is24HourFormat(getActivity()));
	}

	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		mListener.setTime(hourOfDay, minute);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (TimePickerListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement DatePickerListener");
		}
	}
}
