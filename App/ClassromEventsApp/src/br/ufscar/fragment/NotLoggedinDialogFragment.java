package br.ufscar.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import br.ufscar.classromeventsapp.R;

public class NotLoggedinDialogFragment extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setTitle(R.string.notLoggedinTitle).setMessage(R.string.notLoggedinMessage);
		
		builder.setPositiveButton(R.string.ok,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				NotLoggedinDialogFragment.this.getDialog().dismiss();
			}
		});
		
		return builder.create();
	}
}
