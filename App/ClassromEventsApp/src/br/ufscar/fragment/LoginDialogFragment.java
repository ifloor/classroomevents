package br.ufscar.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import br.ufscar.classromeventsapp.R;

public class LoginDialogFragment extends DialogFragment {

	public interface LoginDialogListener {
		public void login(String username, String password);
	}

	private LoginDialogListener mListener;
	private View loginView;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();

		loginView = inflater.inflate(R.layout.activity_login_popup, null);
		
		builder.setView(loginView);

		builder.setPositiveButton(R.string.signin,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						
						EditText username = (EditText) loginView.findViewById(R.id.username);
						EditText password = (EditText) loginView.findViewById(R.id.password);
						
						mListener.login(username.getText().toString(), password.getText().toString());
					}
				}).setNegativeButton(R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						LoginDialogFragment.this.getDialog().dismiss();
					}
				});

		
		
		return builder.create();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		// Verify that the host activity implements the callback interface
		try {
			// Instantiate the NoticeDialogListener so we can send events to the
			// host
			mListener = (LoginDialogListener) activity;
		} catch (ClassCastException e) {
			// The activity doesn't implement the interface, throw exception
			throw new ClassCastException(activity.toString()
					+ " must implement LoginDialogListener");
		}
	}
}
