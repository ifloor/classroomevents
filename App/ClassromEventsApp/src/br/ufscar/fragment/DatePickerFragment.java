package br.ufscar.fragment;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {

	public interface DatePickerListener {
		public void setDate(int year, int month, int day);
	}
	
	public static DatePickerFragment newInstance(Calendar c) {
		DatePickerFragment f = new DatePickerFragment();

		Bundle args = new Bundle();
		args.putInt("year", c.get(Calendar.YEAR));
		args.putInt("month", c.get(Calendar.MONTH));
		args.putInt("day", c.get(Calendar.DAY_OF_MONTH));
		f.setArguments(args);

		return f;
	}

	private DatePickerListener mListener;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		int year = getArguments().getInt("year");
		int month = getArguments().getInt("month");
		int day = getArguments().getInt("day");

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	public void onDateSet(DatePicker view, int year, int month, int day) {
		mListener.setDate(year, month, day);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (DatePickerListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement DatePickerListener");
		}
	}
}
