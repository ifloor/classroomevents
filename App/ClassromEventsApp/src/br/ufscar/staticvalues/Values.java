package br.ufscar.staticvalues;

public class Values {
	public static final String INSTITUTION_URL = "http://do.igormaldonado.com.br/rest/v1/institution";
	public static final String DEPARTMENT_URL = "http://do.igormaldonado.com.br/rest/v1/department";
	public static final String FLOOR_URL = "http://do.igormaldonado.com.br/rest/v1/floor";
	public static final String ROOM_URL = "http://do.igormaldonado.com.br/rest/v1/room";
	public static final String DO_RESERVE_URL = "http://do.igormaldonado.com.br/rest/v1/reserve/";
	public static final String RESERVE_URL = "http://do.igormaldonado.com.br/rest/v1/reserve/room/";
	public static final String PENDENT_RESERVE_URL = "http://do.igormaldonado.com.br/rest/v1/reserve/pendent/";
	public static final String LOGIN_URL = "http://do.igormaldonado.com.br/rest/v1/user/login";
	
	public static final String JSON_TYPE = "application/json";
	public static final String TEXT_TYPE = "text/plain";
	
}
