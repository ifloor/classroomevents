package br.ufscar.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import br.ufscar.app.MapsFragment;
import br.ufscar.app.OpcoesFragment;

public class AppSectionsPagerAdapter extends FragmentPagerAdapter {

    public AppSectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:               
                return new MapsFragment();
            
            //case 1:
            	//return new BuscasFragment();
            	
            case 1:
            	return new OpcoesFragment();
        }
		return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
    	if (position == 0)
    		return "Home";
    	//if (position == 1)
    		//return "Buscas";
    	if (position == 1)
    		return "Op��es";
    	
    	return null; 
    }
}
