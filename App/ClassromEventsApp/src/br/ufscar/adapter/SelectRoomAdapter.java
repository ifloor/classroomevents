package br.ufscar.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import br.ufscar.classromeventsapp.R;
import br.ufscar.model.Room;

public class SelectRoomAdapter extends BaseAdapter{
	private List<Room> lstRoom;
	Context context;

	//Classe utilizada para instanciar os objetos do XML
	private LayoutInflater inflater;

	public SelectRoomAdapter(Context context, List<Room> rooms) {
		this.lstRoom = rooms;
		this.context = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void addItem(final Room item) {
		this.lstRoom.add(item);
		//Atualizar a lista caso seja adicionado algum item
		notifyDataSetChanged();
	}    

	@Override
	public int getCount() {
		return lstRoom.size();
	}

	@Override
	public Object getItem(int position) {
		return lstRoom.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int positon, View convertView, ViewGroup viewGroup) {
		//Pega o registro da lista e trasnfere para o objeto 
		Room room = lstRoom.get(positon);

		//O ViewHolder ir� guardar a inst�ncias dos objetos do produto_row
		ViewHolder holder;

		//Quando o objeto convertView n�o for nulo n�s n�o precisaremos inflar
		//os objetos do XML, ele ser� nulo quando for a primeira vez que for carregado
		if (convertView == null) {

			//Utiliza o XML estado_row para apresentar na tela
			convertView = inflater.inflate(R.layout.list_row_room, null);
			holder = new ViewHolder();

			//Inst�ncia os objetos do XML
			holder.text1 = (TextView)convertView.findViewById(R.id.text1);
			holder.text2 = (TextView)convertView.findViewById(R.id.text2);
			holder.imgIcon = (ImageView)convertView.findViewById(R.id.list_image);
			holder.imgArrow = (ImageView)convertView.findViewById(R.id.imageView1);
			

			convertView.setTag(holder);
		} else {
			//pega o ViewHolder para ter um acesso r�pido aos objetos do XML
			//ele sempre passar� por aqui quando,por exemplo, for efetuado uma rolagem na tela 
			holder = (ViewHolder) convertView.getTag();
		}
		
		//pega os dados que est�o no objeto prod e transfere para os objetos do XML
		holder.text1.setText(room.getName());
		holder.text2.setText(String.valueOf(room.getFloorName()));
		holder.imgIcon.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_room));
		holder.imgArrow.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_arrow));
		
		return convertView;
	}
	//Criada esta classe est�tica para guardar a refer�ncia dos objetos abaixo
	static class ViewHolder {
		public TextView text1, text2;
		public ImageView imgIcon, imgArrow;
		
		public Button testButton;
	}
}
