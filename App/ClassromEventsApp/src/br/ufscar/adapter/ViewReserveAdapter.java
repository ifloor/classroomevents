package br.ufscar.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.ufscar.classromeventsapp.R;
import br.ufscar.model.Reserve;


public class ViewReserveAdapter extends BaseAdapter{
	private List<Reserve> lstReserves;
	private SimpleDateFormat df;
	Context context;

	//Classe utilizada para instanciar os objetos do XML
	private LayoutInflater inflater;

	public ViewReserveAdapter(Context context, List<Reserve> reserves) {
		this.lstReserves = reserves;
		this.context = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		df = new SimpleDateFormat("dd/MM/yyyy");
	}

	public void addItem(final Reserve item) {
		this.lstReserves.add(item);
		//Atualizar a lista caso seja adicionado algum item
		notifyDataSetChanged();
	}    

	@Override
	public int getCount() {
		return lstReserves.size();
	}

	@Override
	public Object getItem(int position) {
		return lstReserves.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int positon, View convertView, ViewGroup viewGroup) {
		//Pega o registro da lista e trasnfere para o objeto 
		Reserve reserve = lstReserves.get(positon);

		//O ViewHolder ir� guardar a inst�ncias dos objetos do produto_row
		ViewHolder holder;

		//Quando o objeto convertView n�o for nulo n�s n�o precisaremos inflar
		//os objetos do XML, ele ser� nulo quando for a primeira vez que for carregado
		if (convertView == null) {

			//Utiliza o XML estado_row para apresentar na tela
			convertView = inflater.inflate(R.layout.list_row_reserve, null);
			holder = new ViewHolder();

			//Inst�ncia os objetos do XML
			holder.text1 = (TextView)convertView.findViewById(R.id.text1);
			holder.text2 = (TextView)convertView.findViewById(R.id.text2);
			holder.text3 = (TextView)convertView.findViewById(R.id.text3);
			holder.text4 = (TextView)convertView.findViewById(R.id.text4);
			holder.imgIcon = (ImageView)convertView.findViewById(R.id.list_image);
			

			convertView.setTag(holder);
		} else {
			//pega o ViewHolder para ter um acesso r�pido aos objetos do XML
			//ele sempre passar� por aqui quando,por exemplo, for efetuado uma rolagem na tela 
			holder = (ViewHolder) convertView.getTag();
		}
		
		//pega os dados que est�o no objeto prod e transfere para os objetos do XML
		holder.text1.setText(reserve.getTeacherName());
		holder.text2.setText(getDurationBreakdown(reserve.getBeginMillis()));
		holder.text3.setText(getDurationBreakdown(reserve.getEndMillis()));
		holder.text4.setText(df.format(new Date(reserve.getBeginMillis())));
		holder.imgIcon.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_schoolar));
		
		return convertView;
	}
	//Criada esta classe est�tica para guardar a refer�ncia dos objetos abaixo
	static class ViewHolder {
		public TextView text1, text2, text3, text4;
		public ImageView imgIcon, imgArrow;
	}
	
	public static String getDurationBreakdown(long millis)
    {
        if(millis < 0)
        {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        

        StringBuilder sb = new StringBuilder(64);        
        sb.append(hours);
        sb.append(":");
        sb.append(minutes);
        sb.append(":");
        sb.append(seconds);        

        return(sb.toString());
    }
}
