package br.ufscar.utils;

public class Utils {

	public static boolean stringIsNullOrEmpty(String s){
		return (s == null || s.isEmpty() || s.equalsIgnoreCase("null"));
	}
}
