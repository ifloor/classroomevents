package br.ufscar.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import br.ufscar.model.Department;
import br.ufscar.model.Floor;
import br.ufscar.model.Institution;
import br.ufscar.model.Room;
import br.ufscar.model.Reserve;

import com.google.android.gms.maps.model.LatLng;

public class ParseJson {
	
	public static List<Institution> getInstitutions(String jsonString) {
		List<Institution> ret = new ArrayList<Institution>();

		if (jsonString == null || jsonString.isEmpty()) {
			return ret;
		}

		try {
			JSONArray insttsList = new JSONArray(jsonString);
			JSONObject instts;

			for (int i = 0; i < insttsList.length(); i++) {
				Institution ins = new Institution();
				instts = new JSONObject(insttsList.getString(i));
				
				ins.setId(Long.parseLong(instts.getString("id")));
				ins.setName(instts.getString("name"));
				ins.setXa(Double.parseDouble(instts.getString("xa")));
				ins.setXb(Double.parseDouble(instts.getString("xb")));
				ins.setYa(Double.parseDouble(instts.getString("ya")));
				ins.setYb(Double.parseDouble(instts.getString("yb")));
				
				ret.add(ins);
			}

		} catch (JSONException e) {
			Log.e("UFSCar", "Erro no parsing do JSON", e);
		}

		return ret;
	}

	public static List<Department> getDepartments(String jsonString) {

		List<Department> ret = new ArrayList<Department>();

		if (jsonString == null || jsonString.isEmpty()) {
			return ret;
		}

		try {
			JSONArray departmentList = new JSONArray(jsonString);

			JSONObject department;

			for (int i = 0; i < departmentList.length(); i++) {
				department = new JSONObject(departmentList.getString(i));
				Department obj = new Department();
				obj.setId(Long.parseLong(department.getString("id")));
				obj.setName(department.getString("name"));
				

				String xa = department.getString("xa");
				String ya = department.getString("ya");
				String xb = department.getString("xb");
				String yb = department.getString("yb");

				if (!Utils.stringIsNullOrEmpty(xa)
						&& !Utils.stringIsNullOrEmpty(ya)
						&& !Utils.stringIsNullOrEmpty(xb)
						&& !Utils.stringIsNullOrEmpty(yb)) {
					obj.setFarRight(new LatLng(Double.parseDouble(xa), Double
							.parseDouble(ya)));
					obj.setNearLeft(new LatLng(Double.parseDouble(xb), Double
							.parseDouble(yb)));
				}
				
				

				ret.add(obj);
			}
		} catch (JSONException e) {
			Log.e("UFSCar", "Erro no parsing do JSON", e);
		}

		return ret;
	}

	public static List<Floor> getFloors(String jsonString) {
		List<Floor> ret = new ArrayList<Floor>();
		List<Department> dept = new ArrayList<Department>();

		if (jsonString == null || jsonString.isEmpty()) {
			return ret;
		}

		try {
			JSONArray floorList = new JSONArray(jsonString);
			JSONObject floor;

			for (int i = 0; i < floorList.length(); i++) {
				Floor f = new Floor();
				floor = new JSONObject(floorList.getString(i));
				f.setId(Long.parseLong(floor.getString("id")));
				f.setName(floor.getString("name"));				
				
				dept = ParseJson.getDepartments("["+floor.getString("department")+"]");
				if(dept.size() > 0)
					f.setDepartment(dept.get(0));
				
				ret.add(f);
			}

		} catch (JSONException e) {
			Log.e("UFSCar", "Erro no parsing do JSON", e);
		}

		return ret;
	}

	public static List<Room> getRooms(String jsonString) {
		List<Room> ret = new ArrayList<Room>();

		if (jsonString == null || jsonString.isEmpty()) {
			return ret;
		}

		try {
			JSONArray floorList = new JSONArray(jsonString);
			JSONObject floor;

			for (int i = 0; i < floorList.length(); i++) {
				Room f = new Room();
				floor = new JSONObject(floorList.getString(i));
				f.setId(Long.parseLong(floor.getString("id")));
				f.setName(floor.getString("name"));

				String xa = floor.getString("xa");
				String ya = floor.getString("ya");
				String xb = floor.getString("xb");
				String yb = floor.getString("yb");

				if (!Utils.stringIsNullOrEmpty(xa)
						&& !Utils.stringIsNullOrEmpty(ya)
						&& !Utils.stringIsNullOrEmpty(xb)
						&& !Utils.stringIsNullOrEmpty(yb)) {
					f.setFarRight(new LatLng(Double.parseDouble(xa), Double
							.parseDouble(ya)));
					f.setNearLeft(new LatLng(Double.parseDouble(xb), Double
							.parseDouble(yb)));
				}

				ret.add(f);
			}

		} catch (JSONException e) {
			Log.e("UFSCar", "Erro no parsing do JSON", e);
		}

		return ret;
	}

	public static List<Reserve> getReserves(String jsonString) {
		List<Reserve> ret = new ArrayList<Reserve>();

		if (jsonString == null || jsonString.isEmpty()) {
			return ret;
		}

		try {
			JSONArray roomList = new JSONArray(jsonString);
			JSONObject room;

			for (int i = 0; i < roomList.length(); i++) {
				Reserve f = new Reserve();
				room = new JSONObject(roomList.getString(i));
				f.setTeacherName(room.getString("teacherName"));
				f.setBeginMillis(Long.valueOf(room.getString("beginMillis")));
				f.setEndMillis(Long.valueOf(room.getString("endMillis")));
				ret.add(f);
			}

		} catch (JSONException e) {
			Log.e("UFSCar", "Erro no parsing do JSON", e);
		}

		return ret;
	}
	
	public static String[] getLogin(String jsonString) {
		if (jsonString == null || jsonString.isEmpty()) {
			return null;
		}
		try {
			JSONObject loginData = new JSONObject(jsonString);

			String ret[] = new String[5];

			ret[0] = loginData.getString("ownerUsername");
			ret[1] = loginData.getString("value");
			ret[2] = loginData.getString("ttl");
			ret[3] = loginData.getString("fullName");
			ret[4] = loginData.getString("type");
			if (!Utils.stringIsNullOrEmpty(ret[0])
					&& !Utils.stringIsNullOrEmpty(ret[1])
					&& !Utils.stringIsNullOrEmpty(ret[2])
					&& !Utils.stringIsNullOrEmpty(ret[3])
					&& !Utils.stringIsNullOrEmpty(ret[4]))
				return ret;

		} catch (JSONException e) {
			Log.e("UFSCar", "Erro no parsing do JSON", e);
		}

		return null;
	}
}
