package br.ufscar.utils;

import java.util.List;

import br.ufscar.model.Department;
import br.ufscar.model.Floor;
import br.ufscar.model.Institution;
import br.ufscar.model.Login;
import br.ufscar.model.Reserve;
import br.ufscar.model.Room;
import br.ufscar.staticvalues.Values;

import com.google.android.gms.maps.model.LatLng;

public class ClassroomAPI {

	public static List<Department> getDepts(String institutionId){
		String id = institutionId;
		String params = "?id="+id;
		String res = DownloadJson.get(Values.DEPARTMENT_URL, params, Values.JSON_TYPE);
		List<Department> Departments = ParseJson.getDepartments(res);

		return Departments;		
	}	
	
	public static List<Institution> getInstitution(LatLng userLocation){
		String x = String.valueOf(userLocation.longitude);
		String y = String.valueOf(userLocation.latitude);
		String params = "?x="+x+"&y="+y;
		
		String res = DownloadJson.get(Values.INSTITUTION_URL, params, Values.JSON_TYPE);
		List<Institution> institutions = ParseJson.getInstitutions(res);

		return institutions;		
	}	

	public static List<Floor> getFloors(String departmentId) {
		String jsonString = DownloadJson.post(Values.FLOOR_URL, departmentId, Values.TEXT_TYPE);
		List<Floor> floors = ParseJson.getFloors(jsonString);

		return floors;
	}

	public static List<Room> getRooms(String roomId) {
		String jsonString = DownloadJson.post(Values.ROOM_URL, roomId, Values.TEXT_TYPE);
		List<Room> rooms = ParseJson.getRooms(jsonString);

		return rooms;
	}

	public static List<Reserve> getReserves(String reserveId) {
		String jsonString = DownloadJson.get(Values.RESERVE_URL, reserveId, Values.TEXT_TYPE);
		List<Reserve> reserves = ParseJson.getReserves(jsonString);

		return reserves;
	}
	
	public static List<Reserve> getPendentReserves(String agentId) {
		StringBuilder params = new StringBuilder();
		params.append("?").append("username=").append(Login.getUsername());
		params.append("&").append("token=").append(Login.getToken());
		params.append("&").append("agentId=").append(agentId);
		String jsonString = DownloadJson.get(Values.PENDENT_RESERVE_URL, params.toString(), Values.TEXT_TYPE);
		List<Reserve> reserves = ParseJson.getReserves(jsonString);
		
		return reserves;
	}

	public static String[] login(String username, String password) {
		StringBuilder builder = new StringBuilder();
		builder.append(Values.LOGIN_URL).append("?username=").append(username).append("&password=").append(password);

		String res = DownloadJson.post(builder.toString(), null,  null);

		System.out.println(res);

		return ParseJson.getLogin(res);
	}
	
	// verificar utilização dos metodos abaixo
	
	public static List<Department> getFarBuilds(double Xa, double Ya, double Xb, double Yb){

		StringBuilder params = new StringBuilder();
		params.append("{ \"p1\": { \"x\": ").append(Xa).append(", \"y\": ").append(Ya)
		.append("}, \"p2\": { \"x\": ").append(Xb).append(", \"y\": ").append(Yb)
		.append("}}");

		String res = DownloadJson.post(Values.DEPARTMENT_URL, params.toString(), Values.JSON_TYPE);

		List<Department> Departments = ParseJson.getDepartments(res);

		return Departments;		
	}
	
	public static String reserve(String roomId, Long start, Long end){
		StringBuilder params = new StringBuilder();

		params.append("{\"beginMillis\":").append(start).append(",\"endMillis\":").append(end).append(",\"room\":{\"id\":").append(roomId).append("}}");
		String url = Values.DO_RESERVE_URL + "?username=" + Login.getUsername() + "&token=" + Login.getToken();
		
		
		System.out.println(params.toString());

		String res = DownloadJson.post(url, params.toString(), Values.JSON_TYPE);

		return res;
	}
}
