package br.ufscar.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;

import android.app.ProgressDialog;
import android.os.NetworkOnMainThreadException;
import android.util.Log;

public class DownloadJson {

	ProgressDialog dialog;

	public static String post(String url, String params, String contentType) {

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);

		if (params != null) {
			try {
				StringEntity se = new StringEntity(params);
				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,
						contentType));
				httppost.setEntity(se);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String json = toString(instream);
				instream.close();

				return json;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	//public static String get(String url, String params, String contentType) {
	public static String get(String url, String params, String contentType) {
		HttpClient httpclient = new DefaultHttpClient();
		
		String test = url+params;
		
		// Prepare a request object
		HttpGet httpget = new HttpGet(test); 

		// Execute the request
		HttpResponse response;

		try{
			response = httpclient.execute(httpget);
			// Examine the response status
			Log.i("HttpGET",response.getStatusLine().toString());

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result= toString(instream);
				// now you have the string representation of the HTML request
				instream.close();

				return result;
			}
		}catch(NetworkOnMainThreadException e){
			e.printStackTrace();
			Log.i("ERROOOOOOOOO", e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static String toString(InputStream is) throws IOException {

		byte[] bytes = new byte[1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int lidos;
		while ((lidos = is.read(bytes)) > 0) {
			baos.write(bytes, 0, lidos);
		}
		return new String(baos.toByteArray());
	}

}