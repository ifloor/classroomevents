package br.ufscar.classromeventsapp;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import br.ufscar.adapter.ListDeptsAdapter;
import br.ufscar.app.SelectFloorActivity;
import br.ufscar.model.Department;
import br.ufscar.model.Institution;
import br.ufscar.utils.ClassroomAPI;
import br.ufscar.utils.GPSTracker;

import com.google.android.gms.maps.model.LatLng;

public class ListDeptsActivity extends ListActivity {

	private ProgressBar pb;
	private ListView lv;
	private GPSTracker gps;	
	private LatLng location;
	private List<Department> detpsList;
	private String universidade;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_depts);

		pb = (ProgressBar)findViewById(R.id.progressBar);
		lv = (ListView)findViewById(android.R.id.list);

		try{			
			location = capturarCoordenadas();
			new DeptsTask().execute(location);
		}
		catch(Exception e){
			e.printStackTrace();
		}				
	}

	private class DeptsTask extends AsyncTask<LatLng, Integer, List<Department>>{
		@Override
		protected List<Department> doInBackground(LatLng... params) {	    	
			return getDepts(params[0]);
		}

		@Override
		protected void onPostExecute(List<Department> detps) {
			ListDeptsAdapter adapter = new ListDeptsAdapter(getApplicationContext(), detps, universidade);
			setListAdapter(adapter);
			
			detpsList = detps;			
						
			pb.setVisibility(View.GONE);
			lv.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onProgressUpdate(Integer... progress){
			pb.setProgress(progress[0]);
		}
	}

	private List<Department> getDepts(LatLng userLocation) {

		// ponto teste dentro na universidade (para testar de casa) -21.979185,-47.878861
		LatLng ufscar = new LatLng(-21.979185, -47.878861);
		List<Institution> instts = ClassroomAPI.getInstitution(ufscar);

		//List<Institution> instts = ClassroomAPI.getInstitution(userLocation);
		String instID = instts.get(0).getId().toString();  
		universidade = instts.get(0).getName();

		List<Department> depts =  ClassroomAPI.getDepts(instID);
		return depts;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		Long deptId = detpsList.get(position).getId();

		Bundle caixa = new Bundle();
		caixa.putLong("DeptId", deptId);
		
		Intent i = new Intent(getApplicationContext(), SelectFloorActivity.class);
		i.putExtra("bundle", caixa);
		startActivity(i);

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_building, menu);
		return true;
	}

	public LatLng capturarCoordenadas(){
		double latitude=0, longitude=0;

		gps = new GPSTracker(getApplicationContext());		

		// check if user's GPS is enabled
		if(gps.canGetLocation()){
			latitude = gps.getLatitude();
			longitude = gps.getLongitude();	

		}else{
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

		return new LatLng(latitude, longitude);
	}

}
