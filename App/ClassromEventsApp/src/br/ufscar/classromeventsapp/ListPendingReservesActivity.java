package br.ufscar.classromeventsapp;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import br.ufscar.adapter.ViewReserveAdapter;
import br.ufscar.model.Login;
import br.ufscar.model.Reserve;
import br.ufscar.utils.ClassroomAPI;

public class ListPendingReservesActivity extends ListActivity {

	private ProgressBar pb;
	private ListView lv;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_reserve);

		pb = (ProgressBar)findViewById(R.id.progressBar);
		lv = (ListView)findViewById(android.R.id.list);

		Intent intent = this.getIntent();		
		Bundle content = intent.getExtras();
		String roomId = (String) content.getString("roomId");
		
		String teste = Login.getUsername();
		
		if(roomId != null)
			new ReserveTask().execute(roomId);
	}

	private class ReserveTask extends AsyncTask<String, Integer, List<Reserve>>{
		@Override
		protected List<Reserve> doInBackground(String... params) {
			return getReserves(params[0]);
		}

		@Override
		protected void onPostExecute(List<Reserve> reserves) {
			ViewReserveAdapter adapter = new ViewReserveAdapter(getApplicationContext(), reserves); 
									
			setListAdapter(adapter);
			
			pb.setVisibility(View.GONE);
			lv.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onProgressUpdate(Integer... progress){
			pb.setProgress(progress[0]);
		}
	}

	private List<Reserve> getReserves(String roomId) {
		List<Reserve> reserves = ClassroomAPI.getReserves(roomId);		
		return reserves;
	}

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_room, menu);
		return true;
	}


}
