package br.ufscar.classromeventsapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import br.ufscar.fragment.DatePickerFragment;
import br.ufscar.fragment.DatePickerFragment.DatePickerListener;
import br.ufscar.fragment.ReserveFailedDialogFragment;
import br.ufscar.fragment.TimePickerFragment;
import br.ufscar.fragment.TimePickerFragment.TimePickerListener;
import br.ufscar.utils.ClassroomAPI;

public class SelectTimeActivity extends FragmentActivity implements DatePickerListener, TimePickerListener{
	private TextView txtDateStart, txtTimeStart, txtDateEnd, txtTimeEnd;
	private Calendar timeStart, timeEnd;
	private Button reserve;
	private String roomId, requestMessage;
	private SimpleDateFormat sdfDate;
	private SimpleDateFormat sdfTime;
	private boolean changingTimeStart, changingDateStart, changingTimeEnd,
			changingDateEnd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_time);

		sdfDate = (SimpleDateFormat) SimpleDateFormat.getDateInstance();
		sdfTime = (SimpleDateFormat) SimpleDateFormat.getTimeInstance();

		txtDateStart = (TextView) findViewById(R.id.txtDateStart);
		txtTimeStart = (TextView) findViewById(R.id.txtTimeStart);
		txtDateEnd = (TextView) findViewById(R.id.txtDateEnd);
		txtTimeEnd = (TextView) findViewById(R.id.txtTimeEnd);

		reserve = (Button) findViewById(R.id.btnReserve);

		Intent intent = this.getIntent();
		Bundle content = intent.getExtras();
		roomId = content.getString("roomId");

		setCurrentTimeAndDate();
		setListeners();

	}

	private void setCurrentTimeAndDate() {
		timeStart = Calendar.getInstance();
		timeEnd = Calendar.getInstance();

		timeEnd.add(Calendar.HOUR_OF_DAY, 4);
	}

	private void setListeners() {

		txtDateStart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				DialogFragment newFragment = DatePickerFragment
						.newInstance(timeStart);
				newFragment.show(getFragmentManager(), "timePicker");
				changingDateStart = true;
			}
		});
		
		txtTimeStart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogFragment newFragment = TimePickerFragment
						.newInstance(timeStart);
				newFragment.show(getFragmentManager(), "timePicker");
				changingTimeStart = true;
			}
		});

		txtDateEnd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogFragment newFragment = DatePickerFragment
						.newInstance(timeEnd);
				newFragment.show(getFragmentManager(), "timePicker");
				changingDateEnd = true;
			}
		});

		txtTimeEnd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogFragment newFragment = TimePickerFragment
						.newInstance(timeEnd);
				newFragment.show(getFragmentManager(), "timePicker");
				changingTimeEnd = true;
			}
		});

		reserve.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new ReserveTask().execute();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_time, menu);
		return true;
	}

	@Override
	public void setTime(int hourOfDay, int minute) {
		if (changingTimeStart) {
			timeStart.set(Calendar.HOUR_OF_DAY, hourOfDay);
			timeStart.set(Calendar.MINUTE, minute);
			txtTimeStart.setText(sdfTime.format(timeStart.getTime()));
			changingTimeStart = false;
		}

		if (changingTimeEnd) {
			timeEnd.set(Calendar.HOUR_OF_DAY, hourOfDay);
			timeEnd.set(Calendar.MINUTE, minute);
			txtTimeEnd.setText(sdfTime.format(timeEnd.getTime()));
			changingTimeEnd = false;
		}
	}

	@Override
	public void setDate(int year, int month, int day) {
		if (changingDateStart) {
			timeStart.set(Calendar.YEAR, year);
			timeStart.set(Calendar.MONTH, month);
			timeStart.set(Calendar.DAY_OF_MONTH, day);
			txtDateStart.setText(sdfDate.format(timeStart.getTime()));
			changingDateStart = false;
		}

		if (changingDateEnd) {
			timeEnd.set(Calendar.YEAR, year);
			timeEnd.set(Calendar.MONTH, month);
			timeEnd.set(Calendar.DAY_OF_MONTH, day);
			txtDateEnd.setText(sdfDate.format(timeEnd.getTime()));
			changingDateEnd = false;
		}
	}

	private Boolean reserveRoom() {
		String res = ClassroomAPI.reserve(roomId, timeStart.getTimeInMillis(),
				timeEnd.getTimeInMillis());
		requestMessage = res;

		return res.equals("ok");
	}

	private class ReserveTask extends AsyncTask<String, Integer, Boolean> {
		@Override
		protected Boolean doInBackground(String... params) {
			return reserveRoom();
		}

		@Override
		protected void onPostExecute(Boolean reserved) {
			if (reserved) {
				// TODO return to first screen
				System.out.println("Reserva realiza com sucesso");
			} else {
				System.out.println("Reserva nao realizadas");
				ReserveFailedDialogFragment f = ReserveFailedDialogFragment.newInstance(requestMessage);
				f.show(getFragmentManager(), "ReseveFailed");
			}
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {

		}
	}

}
