package br.ufscar.model;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;

public class Department {

	private Long id;
	private String name;
	private LatLng farRight, nearLeft;
	private Institution institution;
	private List<Floor> floors;	
	private List<Secretary> secretaries;
	
	public Department(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public List<Floor> getFloors() {
		return floors;
	}

	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public LatLng getFarRight() {
		return farRight;
	}

	public void setFarRight(LatLng farRight) {
		this.farRight = farRight;
	}

	public LatLng getNearLeft() {
		return nearLeft;
	}

	public void setNearLeft(LatLng nearLeft) {
		this.nearLeft = nearLeft;
	}

	public List<Secretary> getSecretaries() {
		return secretaries;
	}

	public void setSecretaries(List<Secretary> secretaries) {
		this.secretaries = secretaries;
	}
}
