package br.ufscar.model;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;

public class Room {

	private long id;
	private String name;
	private LatLng nearLeft, farRight;
	private String floorName;
	private Floor floor;
	private List<Secretary> secretaries;
	private List<Reserve> reserves;
	private List<ResourceRoom> resourceRooms;
	
	public Room(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Floor getFloor() {
		return floor;
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

	public LatLng getNearLeft() {
		return nearLeft;
	}

	public void setNearLeft(LatLng nearLeft) {
		this.nearLeft = nearLeft;
	}

	public LatLng getFarRight() {
		return farRight;
	}

	public void setFarRight(LatLng farRight) {
		this.farRight = farRight;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Secretary> getSecretaries() {
		return secretaries;
	}

	public void setSecretaries(List<Secretary> secretaries) {
		this.secretaries = secretaries;
	}

	public Long getId() {
		return id;
	}

	public List<Reserve> getReserves() {
		return reserves;
	}

	public void setReserves(List<Reserve> reserves) {
		this.reserves = reserves;
	}

	public List<ResourceRoom> getResourceRooms() {
		return resourceRooms;
	}

	public void setResourceRooms(List<ResourceRoom> resourceRooms) {
		this.resourceRooms = resourceRooms;
	}
	
	@Override
	public String toString(){
		return name;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}
}
