package br.ufscar.model;

import java.util.Date;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import br.ufscar.utils.ClassroomAPI;

public class Login {
	
	public interface LoginResultListener {
		public void loginResult(boolean result);
	}
	
	private static String username;
	private static String fullName;
	private static String token;
	private static Date ttl;
	
	private static boolean isAdmin;
	private static boolean isTeacher;
	private static boolean isStudent;
	private static boolean isSecretary;
	
	private static final String ADMIN = "admin";
	private static final String TEACHER = "teacher";
	private static final String STUDENT = "student";
	private static final String SECRETARY = "secretary";
	
	private static LoginResultListener mListener;
	
	public static String getUsername() {
		return username;
	}

	public static String getToken() {
		return token;
	}

	public static Date getTtl() {
		return ttl;
	}

	public static String getFullName() {
		return fullName;
	}

	public static boolean isTeacher() {
		return isTeacher;
	}

	public static boolean isAdmin() {
		return isAdmin;
	}

	public static boolean isStudent() {
		return isStudent;
	}

	public static boolean isSecretary() {
		return isSecretary;
	}

	public static LoginResultListener getmListener() {
		return mListener;
	}

	public static boolean isLoggedin(){
		if(username != null && token != null && ttl != null){
			return (ttl.compareTo(new Date()) > 0);
		}
		
		return false;
	}
	
	public static void signin(Activity activity, String username, String password) {
		validActivity(activity);
		LoginTask loginTask = new LoginTask();
		loginTask.execute(username, password);
	}
	
	private static void validActivity(Activity activity) {
		try {
			mListener = (LoginResultListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement LoginResultListener");
		}
	}

	private static class LoginTask extends AsyncTask<String, Integer, Boolean> {
		
		@Override
		protected Boolean doInBackground(String... params) {
			
			String ret[] = ClassroomAPI.login(params[0], params[1]);
			try {
				ttl = new Date(Long.parseLong(ret[2]));
				token = ret[1];
				username = ret[0];
				fullName = ret[3];
				String type = ret[4];
				
				isAdmin = ADMIN.equalsIgnoreCase(type);
				isTeacher = TEACHER.equalsIgnoreCase(type);
				isStudent = STUDENT.equalsIgnoreCase(type);
				isSecretary = SECRETARY.equalsIgnoreCase(type);
				
				return true;
			}catch(Exception e){
				Log.e("UFSCar", "Error in login", e);
			}
			
			return false;
		}

		@Override
		protected void onPostExecute(Boolean logged) {
			if(!logged) {
				username = null;
				token = null;
				ttl = null;
				fullName = null;
				
				isAdmin = isSecretary = isStudent = isTeacher = false;
			}
			mListener.loginResult(logged);
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {

		}
	}
}
