package br.ufscar.model;

import java.io.Serializable;
import java.util.List;


public class Floor implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Department department;
	private List<Room> rooms;
	private List<Secretary> secretaries;
	
	public Floor(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public List<Secretary> getSecretaries() {
		return secretaries;
	}

	public void setSecretaries(List<Secretary> secretaries) {
		this.secretaries = secretaries;
	}

	public Long getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
