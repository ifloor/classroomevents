package br.ufscar.model;

import java.util.List;

public class Institution {

	

	private Long id;
	private String name;
	private Double xa;
	private Double xb;
	private Double ya;
	private Double yb;
	private List<Department> departments;
	private List<Secretary> secretaries;
	
	public Institution(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getXa() {
		return xa;
	}

	public void setXa(Double xa) {
		this.xa = xa;
	}

	public Double getXb() {
		return xb;
	}

	public void setXb(Double xb) {
		this.xb = xb;
	}

	public Double getYa() {
		return ya;
	}

	public void setYa(Double ya) {
		this.ya = ya;
	}

	public Double getYb() {
		return yb;
	}

	public void setYb(Double yb) {
		this.yb = yb;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}
	

	public List<Secretary> getSecretaries() {
		return secretaries;
	}

	public void setSecretaries(List<Secretary> secretaries) {
		this.secretaries = secretaries;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
	
	@Override
	public String toString(){
		return name;
	}
}
