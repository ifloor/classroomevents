package br.ufscar.model;

import android.annotation.SuppressLint;
import java.util.Date;

public class Reserve {

	private Long id;
	private Long beginMillis;
	private Long endMillis;
	private String teacherName;
	private Date requestedIn;
	private Boolean reserveGranted;
	private Long userReserved;
	private Room room;
	private Teacher teacher;
	
	public Reserve(){
		
	}

	public Long getBeginMillis() {
		return beginMillis;
	}

	@SuppressLint("DefaultLocale")
	public void setBeginMillis(Long beginMillis) {		
		this.beginMillis = beginMillis;
	}

	public Long getEndMillis() {		
		return endMillis;
	}

	@SuppressLint("DefaultLocale")
	public void setEndMillis(Long endMillis) {		
		this.endMillis = endMillis;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Date getRequestedIn() {
		return requestedIn;
	}

	public void setRequestedIn(Date requestedIn) {
		this.requestedIn = requestedIn;
	}

	public Boolean getReserveGranted() {
		return reserveGranted;
	}

	public void setReserveGranted(Boolean reserveGranted) {
		this.reserveGranted = reserveGranted;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Long getId() {
		return id;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Long getUserReserved() {
		return userReserved;
	}

	public void setUserReserved(Long userReserved) {
		this.userReserved = userReserved;
	}
	
	public String toString(){
		return teacherName;
	}
}
