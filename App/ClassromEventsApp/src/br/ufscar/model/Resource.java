package br.ufscar.model;

import java.util.List;

public class Resource {

	private Long id;
	private String name;
	private String company;
	private String description;
	private String model;
	private List<ResourceRoom> resourceRooms;

	public Resource(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public List<ResourceRoom> getResourceRooms() {
		return resourceRooms;
	}

	public void setResourceRooms(List<ResourceRoom> resourceRooms) {
		this.resourceRooms = resourceRooms;
	}

	public Long getId() {
		return id;
	}
}
