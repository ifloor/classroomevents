package br.ufscar.app;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import br.ufscar.adapter.SelectFloorAdapter;
import br.ufscar.classromeventsapp.R;
import br.ufscar.model.Floor;
import br.ufscar.utils.ClassroomAPI;

public class SelectFloorActivity extends ListActivity {

	private ProgressBar pb;
	private ListView lv;

	private List<Floor> floorList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_floor);

		pb = (ProgressBar)findViewById(R.id.progressBar);
		lv = (ListView)findViewById(android.R.id.list);

		Bundle caixa = getIntent().getParcelableExtra("bundle");
		String deptId = String.valueOf(caixa.getLong("DeptId"));

		if(deptId != null)
			new FloorsTask().execute(deptId);
	}

	private class FloorsTask extends AsyncTask<String, Integer, List<Floor>>{
		@Override
		protected List<Floor> doInBackground(String... params) {
			return getFloors(params[0]);
		}

		@Override
		protected void onPostExecute(List<Floor> floors) {
			SelectFloorAdapter adapter = new SelectFloorAdapter(getApplicationContext(), floors);
			setListAdapter(adapter);
			
			floorList = floors;			
			//setListAdapter(new ArrayAdapter(getApplicationContext(), R.layout.mytextview, floors));
						
			pb.setVisibility(View.GONE);
			lv.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onProgressUpdate(Integer... progress){
			pb.setProgress(progress[0]);
		}
	}

	private List<Floor> getFloors(String departmentId) {
		List<Floor> floors = ClassroomAPI.getFloors(departmentId);		
		return floors;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		String floorId = floorList.get(position).getId().toString();
		String floorName = floorList.get(0).getName();

		Bundle content = new Bundle();
		content.putString("floorId", floorId);
		content.putString("floorName", floorName);
		
		Intent i = new Intent(this, SelectRoomActivity.class);
		i.putExtras(content);
		startActivity(i);  

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_floor, menu);
		return true;
	}
}
