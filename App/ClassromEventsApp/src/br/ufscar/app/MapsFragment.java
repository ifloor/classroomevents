package br.ufscar.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import br.ufscar.classromeventsapp.R;
import br.ufscar.model.Department;
import br.ufscar.model.Institution;
import br.ufscar.utils.ClassroomAPI;
import br.ufscar.utils.GPSTracker;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

//GoogleMaps Key
//AIzaSyAYk-AQWmxdjSGa4BMmSfin-OLZncVXND8

public class MapsFragment extends Fragment  implements OnMapClickListener{
	private GoogleMap googleMap;
	private LatLng location;
	private GPSTracker gps;		
	private Polygon polygon;
	private ProgressBar pb;
	//lista que cont�m os pol�gnos dos departamentos com seus respectivos IDs
	private Map<Long, Polygon> listPolygDept = new HashMap<Long, Polygon>(); 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		
		View rootView = inflater.inflate(R.layout.fragment_maps, container, false);

		pb = (ProgressBar)rootView.findViewById(R.id.progressBarMaps);

		try{			
			location = capturarCoordenadas();
			new MapTask().execute(location);
		}
		catch(Exception e){
			e.printStackTrace();
		}		

		return rootView;
	}

	public LatLng capturarCoordenadas(){
		double latitude=0, longitude=0;

		gps = new GPSTracker(getActivity().getApplicationContext());		

		// check if user's GPS is enabled
		if(gps.canGetLocation()){
			latitude = gps.getLatitude();
			longitude = gps.getLongitude();	

		}else{
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

		return new LatLng(latitude, longitude);
	}

	private class MapTask extends AsyncTask<LatLng, Integer, List<Department>>{
		@Override
		protected List<Department> doInBackground(LatLng... params) {	    	
			return getDepts(params[0]);
		}

		@Override
		protected void onPostExecute(List<Department> depts) {			
			pb.setVisibility(View.GONE);			
			initializeMap(location, depts);
		}

		@Override
		protected void onProgressUpdate(Integer... progress){
			pb.setProgress(progress[0]);
		}
	}

	private List<Department> getDepts(LatLng userLocation) {
		// ponto teste dentro na universidade (para testar de casa) -21.979185,-47.878861
		LatLng ufscar = new LatLng(-21.979185, -47.878861);
		List<Institution> instts = ClassroomAPI.getInstitution(ufscar);
		List<Department> depts = new ArrayList<Department>();

		if(instts.size() > 0){
			//List<Institution> instts = ClassroomAPI.getInstitution(userLocation);
			String instID = instts.get(0).getId().toString();
			depts =  ClassroomAPI.getDepts(instID);
		}

		return depts;
	}

	private void initializeMap(LatLng currentLocation, List<Department> depts) {		

		FragmentManager myFM = getActivity().getSupportFragmentManager();
		final SupportMapFragment myMAPF = (SupportMapFragment) myFM.findFragmentById(R.id.map1);
		googleMap = myMAPF.getMap();

		//googleMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();	
		googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		googleMap.addMarker(new MarkerOptions().position(currentLocation).title("Current Location"));

		// Move a camera para Framework System com zoom 15. 
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation , 15));
		googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);


		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(getActivity().getApplicationContext(),
					"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
		}

		for(Department b : depts){
			polygon = googleMap.addPolygon(new PolygonOptions()
			.add(new LatLng(b.getNearLeft().longitude, b.getNearLeft().latitude), new LatLng(b.getFarRight().longitude, b.getNearLeft().latitude), new LatLng(b.getFarRight().longitude, b.getFarRight().latitude), new LatLng(b.getNearLeft().longitude, b.getFarRight().latitude))
			.strokeColor(Color.RED)
			.strokeWidth(4)
			.fillColor(Color.TRANSPARENT));

			listPolygDept.put(b.getId(), polygon);
		}
		googleMap.setOnMapClickListener(this);
	}		

	@Override
	public void onMapClick(LatLng latLong) {	
		LatLng nearLeft = null, farRight = null;
		boolean hit = false;
		Long deptId = null;

		for(Long id: listPolygDept.keySet()){
			Polygon p = listPolygDept.get(id);

			nearLeft = p.getPoints().get(0);
			farRight = p.getPoints().get(2);

			if ((nearLeft.latitude < latLong.latitude) && (latLong.latitude < farRight.latitude)
					&& (nearLeft.longitude < latLong.longitude) && (latLong.longitude < farRight.longitude)){								
				hit = true;
				deptId = id;
				break;
			}
		}

		if(hit){			
			Toast.makeText(getActivity().getApplicationContext(), 
					"Clicou no lugar certo! ", Toast.LENGTH_SHORT).show();

			Bundle caixa = new Bundle();
			caixa.putLong("DeptId", deptId);

			Intent i = new Intent(getActivity().getApplicationContext(), SelectFloorActivity.class);
			i.putExtra("bundle", caixa);
			startActivity(i);
		}
		else{
			Toast.makeText(getActivity().getApplicationContext(), 
					"N�o h� pr�dios cadastrados onde clicou.", Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		new MapTask().execute(location);
	}

	public void onDestroyView() {
		super.onDestroyView(); 
		gps.stopUsingGPS();

		Fragment fragment = (getFragmentManager().findFragmentById(R.layout.fragment_maps)); 
		Fragment fragment2 = (getFragmentManager().findFragmentById(R.id.map1));
		FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
		ft.remove(fragment);
		ft.remove(fragment2);
		ft.commitAllowingStateLoss();
		
		
	}
}
