package br.ufscar.app;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import br.ufscar.adapter.SelectRoomAdapter;
import br.ufscar.classromeventsapp.R;
import br.ufscar.classromeventsapp.SelectTimeActivity;
import br.ufscar.model.Login;
import br.ufscar.model.Room;
import br.ufscar.utils.ClassroomAPI;

public class SelectRoomActivity extends ListActivity {

	private ProgressBar pb;
	private ListView lv;
	private List<Room> roomList;
	private String floorName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_room);

		pb = (ProgressBar)findViewById(R.id.progressBar);
		lv = (ListView)findViewById(android.R.id.list);

		Intent intent = this.getIntent();		
		Bundle content = intent.getExtras();
		String floorId = (String) content.getString("floorId");		
		floorName = (String)content.getString("floorName");
		
		if(floorId != null)
			new RoomsTask().execute(floorId);
		
	}
	
	OnItemClickListener onItemClick_List = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> arg0, View view, int position, long index) {
			String roomId = roomList.get(position).getId().toString();
			String roomName = roomList.get(position).getName();

			Bundle content = new Bundle();
			content.putString("roomId", roomId);
			content.putString("roomName", roomName);

			if(Login.isStudent()) {
				Intent i = new Intent(getApplicationContext(), ViewReserveActivity.class);
				i.putExtras(content);
				startActivity(i);  
			}
			else if(Login.isTeacher() || Login.isAdmin()){
				Intent i = new Intent(getApplicationContext(), SelectTimeActivity.class);
				i.putExtras(content);
				startActivity(i);  
			}
			else{
				Toast.makeText(getApplicationContext(), "Voc� precisa estar logado para continuar", Toast.LENGTH_SHORT).show();
			}    
		}
	};

	private class RoomsTask extends AsyncTask<String, Integer, List<Room>>{
		@Override
		protected List<Room> doInBackground(String... params) {
			return getRooms(params[0]);
		}

		@Override
		protected void onPostExecute(List<Room> rooms) {
			
			lv.setOnItemClickListener(onItemClick_List);
			for(int i=0; i<rooms.size(); i++)
				rooms.get(i).setFloorName(floorName);
			SelectRoomAdapter adapter = new SelectRoomAdapter(getApplicationContext(), rooms);
			setListAdapter(adapter);
			
			roomList = rooms;
			//setListAdapter(new ArrayAdapter(getApplicationContext(), R.layout.mytextview, rooms));
			pb.setVisibility(View.GONE);
			lv.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onProgressUpdate(Integer... progress){
			pb.setProgress(progress[0]);
		}
	}

	private List<Room> getRooms(String floorId) {
		List<Room> rooms = ClassroomAPI.getRooms(floorId);		
		return rooms;
	}	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_room, menu);
		return true;
	}

}
