package br.ufscar.app;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import br.ufscar.classromeventsapp.R;
import br.ufscar.fragment.LoginDialogFragment;
import br.ufscar.fragment.LoginErrorDialogFragment;
import br.ufscar.fragment.NotLoggedinDialogFragment;
import br.ufscar.fragment.WaitingDialogFragment;
import br.ufscar.model.Login;

public class LoginActivity extends FragmentActivity implements LoginDialogFragment.LoginDialogListener, Login.LoginResultListener {

	
	private Button btnVerMap, btnRoomBooking, btnLogin;
	private DialogFragment waitingDialog;
	//private TextView txtWelcome;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		waitingDialog = new WaitingDialogFragment();

		// link between JavaCode and Layout-XML
		btnVerMap = (Button)findViewById(R.id.btnVerMapa);
		btnRoomBooking = (Button)findViewById(R.id.btnRoomBooking);
		btnLogin = (Button)findViewById(R.id.btnLogin);
		//txtWelcome = (TextView)findViewById(R.id.txtWelcome);

		//setButtons();
		
		loginPopUp();
		       
		btnVerMap.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				interfaceTeste();
			}
		});
		btnRoomBooking.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				roomBooking();
			}
		});
		
		btnLogin.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				loginPopUp();
			}
		});

	}	

	
	public void interfaceTeste(){
		Intent i = new Intent(this, AppMain.class);		
		startActivity(i);                  
	}
	
	public void roomBooking(){
		if(Login.isLoggedin()) {
			Intent i = new Intent(this, SelectFloorActivity.class);
			startActivity(i);
		}else{
			DialogFragment newFragment = new NotLoggedinDialogFragment();
			newFragment.show(getFragmentManager(), "login");
		}
	}
	
	private void loginPopUp(){
		DialogFragment newFragment = new LoginDialogFragment();
		newFragment.show(getFragmentManager(), "login");
	}

	@Override
	public void login(String username, String password){
		
		waitingDialog.show(getFragmentManager(), "waiting");		
		Login.signin(this, username, password);
	}
	
	@Override
	public void loginResult(boolean result){
		waitingDialog.getDialog().dismiss();

		if(result){
			//setButtons();
			interfaceTeste();
		} else {
			DialogFragment newFragment = new LoginErrorDialogFragment();
			newFragment.show(getFragmentManager(), "loginFailed");
		}
	}

		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
}
