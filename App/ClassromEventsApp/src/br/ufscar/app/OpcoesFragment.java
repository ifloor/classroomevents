package br.ufscar.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import br.ufscar.classromeventsapp.ListDeptsActivity;
import br.ufscar.classromeventsapp.ListPendingReservesActivity;
import br.ufscar.classromeventsapp.R;
import br.ufscar.model.Login;

public class OpcoesFragment extends Fragment implements OnClickListener, Login.LoginResultListener{
	private Button btnLogin, btnPredios, btnSolicitacoesReserva, btnLogout;
	private TextView txtWelcome;

	public OpcoesFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_opcoes, container, false);

		btnLogin = (Button)rootView.findViewById(R.id.btnLogin1);
		btnPredios = (Button)rootView.findViewById(R.id.btnPredios);
		btnSolicitacoesReserva = (Button)rootView.findViewById(R.id.btnSolicitacoesReserva);
		txtWelcome = (TextView)rootView.findViewById(R.id.txtWelcome1);
		btnLogout = (Button)rootView.findViewById(R.id.btnLogout);

		setButtons();

		return rootView;
	}

	public void setButtons(){		
		if(Login.isLoggedin()){
			String welcomeMessage = getString(R.string.welcome);
			txtWelcome.setText(welcomeMessage.replace("{0}", Login.getFullName()));
			txtWelcome.setVisibility(View.VISIBLE);
			if (Login.isAdmin()){
				btnLogin.setVisibility(View.GONE);
				btnPredios.setVisibility(View.VISIBLE);
				btnSolicitacoesReserva.setVisibility(View.VISIBLE);
				btnLogout.setVisibility(View.VISIBLE);
			} else if(Login.isTeacher()){
				btnLogin.setVisibility(View.GONE);
				btnPredios.setVisibility(View.VISIBLE);
				btnSolicitacoesReserva.setVisibility(View.GONE);
				btnLogout.setVisibility(View.VISIBLE);
			} else if(Login.isSecretary()) {
				btnLogin.setVisibility(View.GONE);
				btnPredios.setVisibility(View.GONE);
				btnSolicitacoesReserva.setVisibility(View.VISIBLE);
				btnLogout.setVisibility(View.VISIBLE);
			} else if(Login.isStudent()) {
				btnLogin.setVisibility(View.GONE);
				btnPredios.setVisibility(View.GONE);
				btnSolicitacoesReserva.setVisibility(View.GONE);
				btnLogout.setVisibility(View.VISIBLE);
			}
		} else {
			txtWelcome.setVisibility(View.GONE);
			btnLogin.setVisibility(View.VISIBLE);
			btnPredios.setVisibility(View.GONE);
			btnSolicitacoesReserva.setVisibility(View.GONE);
			btnLogout.setVisibility(View.GONE);
		}

		btnLogin.setOnClickListener(this);
		btnPredios.setOnClickListener(this);
		btnSolicitacoesReserva.setOnClickListener(this);
		btnLogout.setOnClickListener(this);

	}

	public void login(){
		Intent i = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
		startActivity(i);
	}

	@Override
	public void loginResult(boolean result) {
		setButtons();	
	}	

	public void departamentos(){
		Intent i = new Intent(getActivity().getApplicationContext(), ListDeptsActivity.class);
		startActivity(i);
	}

	public void solicitacoesReserva(){
		Intent i = new Intent(getActivity().getApplicationContext(), ListPendingReservesActivity.class);
		startActivity(i);
	}
	
	public void logoutUser(){
		Intent i = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
		startActivity(i);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.btnLogin1:
			login();
			break;
		case R.id.btnPredios:
			departamentos();
			break;	
		case R.id.btnSolicitacoesReserva:
			solicitacoesReserva();
			break;
		case R.id.btnLogout:
			logoutUser();
			break;
		}

	}

	public void onDestroyView() {
		super.onDestroyView(); 

		Fragment fragment = (getFragmentManager().findFragmentById(R.layout.fragment_opcoes));   

		FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
		ft.remove(fragment);
		ft.commitAllowingStateLoss();
	}

}