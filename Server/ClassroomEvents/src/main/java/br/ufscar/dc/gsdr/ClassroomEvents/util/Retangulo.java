package br.ufscar.dc.gsdr.ClassroomEvents.util;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Retangulo {
	private Ponto p1, p2;
	
	public Retangulo(){
		
	}

	public Ponto getP1() {
		return p1;
	}

	public void setP1(Ponto p1) {
		this.p1 = p1;
	}

	public Ponto getP2() {
		return p2;
	}

	public void setP2(Ponto p2) {
		this.p2 = p2;
	}

	@Override
	public String toString() {
		return "Retangulo [p1=" + p1 + ", p2=" + p2 + "]";
	}
	
	
	
	
	
}
