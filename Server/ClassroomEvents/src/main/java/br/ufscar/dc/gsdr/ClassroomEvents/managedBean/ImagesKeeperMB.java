package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;

import org.apache.commons.io.FilenameUtils;

import br.ufscar.dc.gsdr.ClassroomEvents.enumeration.Building;
import br.ufscar.dc.gsdr.ClassroomEvents.images.ImageDaemon;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;

@ManagedBean
@SessionScoped
public class ImagesKeeperMB implements Serializable{
	private static final long serialVersionUID = 5290238225159860294L;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	private Agent agent;
	
	
	@EJB private ImageDaemon imgDaemon;
	
	
	private HashMap<String, String> files;
	private String realPath;

	public ImagesKeeperMB(){
		
	}
	
	@PostConstruct
	public void init(){
		agent = loginMB.getAgentLogged();
		files = new HashMap<String, String>();
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();  
        realPath = servletContext.getRealPath("") + File.separator+"images" + File.separator+agent.getId() + File.separator;
        File f = new File(realPath);
        if(!f.exists()){
        	f.mkdirs();
        }else{
        	File[] fs = f.listFiles();
        	for (int i = 0; i < fs.length; i++) {
				fs[i].delete();
				//System.out.println("deleting: "+fs[i].getAbsolutePath());
			}
        }
	}
	
	private String addRegister(Building entity, Long id, String specificFileName){
		File file =  imgDaemon.getImageFile(entity, id,specificFileName);
		
		if(file != null && file.exists()){
			String absoluteFilePath = realPath +hash(entity, id, specificFileName);
			FileImageOutputStream imageOutput;
	        try {
	        	BufferedImage buffImg = ImageIO.read(file);
	    		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    		ImageIO.write(buffImg, FilenameUtils.getExtension(file.getName()), baos);
	    		byte[] imgBytes = baos.toByteArray();
	            imageOutput = new FileImageOutputStream(new File(absoluteFilePath));
	            imageOutput.write(imgBytes, 0, imgBytes.length);
	            imageOutput.close();  
	        }  
	        catch(Exception e) {
	        	e.printStackTrace();
	        }
			files.put(hash(entity, id, specificFileName), getRelativePathByWebAppRoot(entity, id, specificFileName));
			return absoluteFilePath;
		}else{
			return "";
		}
	}
	
//	private String addRegister(Building entity, Long id){
//		return addRegister(entity, id, ImageDaemon.DEFAULT_FILENAME);
//	}
	
	//
	public String getRegister(Building entity, Long id, String specificFileName){
		if(!files.containsKey(hash(entity, id, specificFileName))){
			addRegister(entity, id, specificFileName);
		}
		return files.get(hash(entity, id, specificFileName));
	}
	
	public String getRegister(Building entity, Long id){
		return getRegister(entity, id, ImageDaemon.DEFAULT_FILENAME);
	}
	
	public String getRelativePathByWebAppRoot(Building entity, Long id, String specificFileName){
		return "/images/"+agent.getId().toString()+"/"+hash(entity, id, specificFileName);
	}
	
	
	@PreDestroy
	public void destroy(){
		File f = new File(realPath);
        if(f.exists()){
        	File[] fs = f.listFiles();
        	for (int i = 0; i < fs.length; i++) {
				fs[i].delete();
				//System.out.println("deleting: "+fs[i].getAbsolutePath());
			}
        }
	}
	
	public String hash(Building entity, Long id){
		return hash(entity, id, ImageDaemon.DEFAULT_FILENAME);
	}
	
	public String hash(Building entity, Long id, String specificFileName){
		return entity.toString()+id.toString()+specificFileName;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}
	
	
	
}
