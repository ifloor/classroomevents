package br.ufscar.dc.gsdr.ClassroomEvents.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import br.ufscar.dc.gsdr.ClassroomEvents.util.Cartesian2PointsWidhtHeightObject;
import br.ufscar.dc.gsdr.ClassroomEvents.util.Cartesian4PointsObject;
import br.ufscar.dc.gsdr.ClassroomEvents.util.CoordinatesConverter;

@Entity
public class Room implements Comparable<Room>{
	
	@Transient
	private Cartesian2PointsWidhtHeightObject c2 = null;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private Double xa;
	
	@Column
	private Double xb;
	
	@Column
	private Double ya;
	
	@Column
	private Double yb;
	
	public Long getLeft(){
		if(c2 == null)
			c2 = CoordinatesConverter.to2P(new Cartesian4PointsObject(xa, xb, ya, yb));
		return c2.getLeft();
	}
	
	public Long getTop(){
		if(c2 == null)
			c2 = CoordinatesConverter.to2P(new Cartesian4PointsObject(xa, xb, ya, yb));
		return c2.getTop();
	}
	
	public Long getWidth(){
		if(c2 == null)
			c2 = CoordinatesConverter.to2P(new Cartesian4PointsObject(xa, xb, ya, yb));
		return c2.getWidth();
	}
	
	public Long getHeight(){
		if(c2 == null)
			c2 = CoordinatesConverter.to2P(new Cartesian4PointsObject(xa, xb, ya, yb));
		return c2.getHeight();
	}
	
	
	@ManyToOne
	@JsonIgnore
	private Floor floor;
	
	@ManyToMany
	@JsonIgnore
	private List<Secretary> secretaries;
	
	@OneToMany(mappedBy="room")
	@JsonIgnore
	private List<Reserve> reserves;
	
	@OneToMany(mappedBy="room")
	@JsonIgnore
	private List<ResourceRoom> resourceRooms;
	
	
	public Room(){
		
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}




	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public Double getXa() {
		return xa;
	}

	public void setXa(Double xa) {
		this.xa = xa;
	}

	public Double getXb() {
		return xb;
	}

	public void setXb(Double xb) {
		this.xb = xb;
	}

	public Double getYa() {
		return ya;
	}

	public void setYa(Double ya) {
		this.ya = ya;
	}

	public Double getYb() {
		return yb;
	}

	public void setYb(Double yb) {
		this.yb = yb;
	}

	public Floor getFloor() {
		return floor;
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

	

	public List<Secretary> getSecretaries() {
		return secretaries;
	}

	public void setSecretaries(List<Secretary> secretaries) {
		this.secretaries = secretaries;
	}

	public Long getId() {
		return id;
	}

	public List<Reserve> getReserves() {
		return reserves;
	}

	public void setReserves(List<Reserve> reserves) {
		this.reserves = reserves;
	}

	public List<ResourceRoom> getResourceRooms() {
		return resourceRooms;
	}

	public void setResourceRooms(List<ResourceRoom> resourceRooms) {
		this.resourceRooms = resourceRooms;
	}

	@Override
	public int compareTo(Room o) {
		return id.compareTo(o.getId());
	}

	
	
	
	
}
