package br.ufscar.dc.gsdr.ClassroomEvents.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.DepartmentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.FloorDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.RoomDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Floor;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;

@Path("/floor")
public class FloorService {
	
	@EJB
	FloorDao floorDao;
	
	@EJB DepartmentDao departmentDao;
	@EJB RoomDao rDao;
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	public Response getFloor(String id){
		try{
			List<Floor> floors = floorDao.getFloors(Long.parseLong(id));
			
			return Response.ok(floors).status(Status.OK).build();
		}catch(Exception e){
			return Response.ok(e.getMessage()).status(Status.OK).build();
		}
	}
	
	@Path("/{id}/rooms/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRooms(@PathParam("id") String id){
		try{
			List<Room> rooms = rDao.getFloors(Long.parseLong(id));
			
			return Response.ok(rooms).status(Status.OK).build();
		}catch(Exception e){
			return Response.ok(e.getMessage()).status(Status.OK).build();
		}
	}
	
}
