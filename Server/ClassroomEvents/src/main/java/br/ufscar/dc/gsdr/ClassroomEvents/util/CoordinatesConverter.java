package br.ufscar.dc.gsdr.ClassroomEvents.util;

public class CoordinatesConverter {

	public static Cartesian4PointsObject to4P(Cartesian2PointsWidhtHeightObject obj){
		Cartesian4PointsObject c4 = new Cartesian4PointsObject();
		c4.setXa(obj.getLeft().doubleValue());
		c4.setYa(obj.getTop().doubleValue());
		c4.setXb(obj.getWidth().doubleValue() + c4.getXa().doubleValue());
		c4.setYb(obj.getHeight().doubleValue() + c4.getYa().doubleValue());
		return c4;
	}
	
	public static Cartesian2PointsWidhtHeightObject to2P(Cartesian4PointsObject obj){
		Cartesian2PointsWidhtHeightObject c2 = new Cartesian2PointsWidhtHeightObject();
		c2.setLeft(obj.getXa().longValue());
		c2.setTop(obj.getYa().longValue());
		c2.setHeight(obj.getYb().longValue() - obj.getYa().longValue());
		c2.setWidth(obj.getXb().longValue() - obj.getXa().longValue());
		return c2;
	}
}
