package br.ufscar.dc.gsdr.ClassroomEvents.converter;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.DepartmentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;

@ManagedBean
@RequestScoped
public class DepartmentConverter implements Converter{

	@EJB private DepartmentDao dDao;
	
	@Override
	public Object getAsObject(FacesContext ctx, UIComponent comp, String value) {
		try{Long id = Long.parseLong(value);
			return dDao.findById(id);
		}
		catch(Exception e){
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext ctx, UIComponent comp, Object obj) {
		return ((Department)obj).getId().toString();
	}

}
