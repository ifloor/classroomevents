package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Institution;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;

@Stateless
@TransactionManagement
public class InstitutionDao extends
		HibernateEntityManagerGenericDao<Institution, Long> {

	@EJB
	private DepartmentDao dDao;
	@EJB
	private SecretaryDao sDao;

	public InstitutionDao() {
		super(Institution.class);
	}
	
	public Institution loadInstitutionWithSecretaries(Long institutionId){
		Criteria  crit = getSession().createCriteria(Institution.class);
		crit.add(Restrictions.eq("id", institutionId));
		Institution ins = (Institution)crit.uniqueResult();
		if(ins != null){
			ins.getSecretaries().size();
		}
		return ins;
	}

	@SuppressWarnings("unchecked")
	public List<Institution> findAllOrderedByName() {
		Criteria crit = getSession().createCriteria(Institution.class);
		crit.addOrder(Order.asc("name"));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Institution> getByAgent(Agent agent) {
		if (agent instanceof Admin) {
			return findAllOrderedByName();
		}
		Criteria crit = getSession().createCriteria(Institution.class);
		crit.createAlias("secretaries", "secs");
		crit.add(Restrictions.eq("secs.id", agent.getId()));
		crit.addOrder(Order.asc("name"));
		return crit.list();
	}
	/**
	 * Return only the instituions directly accessed
	 */
	public List<Institution> getByAgentStrictly(Agent agent) {
		return getByAgent(agent);
	}

	@SuppressWarnings("unchecked")
	public void deleteWithDepartments(Long institutionId) {
		List<Department> departments = dDao.getDepartments(institutionId);
		for (Department dep : departments) {
			dDao.deleteWithFloors(dep.getId());
		}

		List<Secretary> secretaries = null;
		Criteria crit = getSession().createCriteria(Secretary.class);
		crit.createAlias("institutions", "i");
		crit.add(Restrictions.eq("i.id", institutionId));
		secretaries = crit.list();
		if (secretaries != null) {
			for (Secretary secretary : secretaries) {
				for (int i = 0; i < secretary.getInstitutions().size(); i++) {
					secretary.getInstitutions().remove(i);
					i--;
				}
				sDao.updateFlushing(secretary);
			}
		}
		Institution i = findById(institutionId);
		i.setSecretaries(null);
		update(i);
		getSession().flush();
		delete(institutionId);
	}

	@SuppressWarnings("unchecked")
	public List<Institution> getInstitution(double x, double y) {
		Criteria crit = getSession().createCriteria(Institution.class);
		crit.add(Restrictions.ge("xa", x));
		crit.add(Restrictions.ge("ya", y));
		crit.add(Restrictions.le("xb", x));
		crit.add(Restrictions.le("yb", y));
		return crit.list();
	}

}
