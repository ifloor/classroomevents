package br.ufscar.dc.gsdr.ClassroomEvents.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class Floor implements Comparable<Floor>{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String name;
	
	
	@ManyToOne
	private Department department;
	
	@OneToMany(mappedBy="floor")
	@JsonIgnore
	private List<Room> rooms;
	
	@ManyToMany
	@JsonIgnore
	private List<Secretary> secretaries;
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Floor other = (Floor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Floor(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public List<Secretary> getSecretaries() {
		return secretaries;
	}

	public void setSecretaries(List<Secretary> secretaries) {
		this.secretaries = secretaries;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int compareTo(Floor o) {
		return id.compareTo(o.getId());
	}
	
	
	
}
