package br.ufscar.dc.gsdr.ClassroomEvents.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class Department implements Comparable<Department>{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private Double xa;
	
	@Column
	private Double xb;
	
	@Column
	private Double ya;
	
	@Column
	private Double yb;
	
	@ManyToOne
	private Institution institution;
	
	@OneToMany(mappedBy="department")
	@JsonIgnore
	private List<Floor> floors;
	
	@ManyToMany
	@JsonIgnore
	private List<Secretary> secretaries;
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Department(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public Double getXa() {
		return xa;
	}

	public void setXa(Double xa) {
		this.xa = xa;
	}

	public Double getXb() {
		return xb;
	}

	public void setXb(Double xb) {
		this.xb = xb;
	}

	public Double getYa() {
		return ya;
	}

	public void setYa(Double ya) {
		this.ya = ya;
	}

	public Double getYb() {
		return yb;
	}

	public void setYb(Double yb) {
		this.yb = yb;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public List<Floor> getFloors() {
		return floors;
	}

	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	public Long getId() {
		return id;
	}

	public List<Secretary> getSecretaries() {
		return secretaries;
	}

	public void setSecretaries(List<Secretary> secretaries) {
		this.secretaries = secretaries;
	}

	@Override
	public int compareTo(Department o) {
		return id.compareTo(o.getId());
	}

	
	
	
	
	
}
