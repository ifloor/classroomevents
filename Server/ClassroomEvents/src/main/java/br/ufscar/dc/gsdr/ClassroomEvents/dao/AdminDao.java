package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;

@Stateless
@TransactionManagement
public class AdminDao extends HibernateEntityManagerGenericDao<Admin, Long>{
	
	public AdminDao(){
		super(Admin.class);
	}
	
	public Admin findByUsername(String username){
		Criteria crit = getSession().createCriteria(Admin.class);
		crit.add(Restrictions.eq("username", username));
		Object obj = crit.uniqueResult();
		if(obj != null){
			return (Admin)obj;
		}else{
			return null;
		}
	}

}
