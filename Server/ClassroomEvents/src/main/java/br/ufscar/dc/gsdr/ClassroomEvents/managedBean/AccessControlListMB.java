package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.DepartmentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.FloorDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.InstitutionDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.RoomDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.SecretaryDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Floor;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Institution;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;
import br.ufscar.dc.gsdr.ClassroomEvents.util.WebTools;

@ManagedBean
@ViewScoped
public class AccessControlListMB implements Serializable{
	private static final long serialVersionUID = 2606398902756099047L;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	@EJB private SecretaryDao sDao;
	@EJB private InstitutionDao iDao;
	@EJB private DepartmentDao dDao;
	@EJB private FloorDao fDao;
	@EJB private RoomDao rDao;
	
	private Agent agentLogged;
	
	private List<Secretary> secretaries;
	
	private Secretary secretary;
	
	private DualListModel<Institution> insitutions;
	private DualListModel<Department> departments;
	private DualListModel<Floor> floors;
	private DualListModel<Room> rooms;
	
	@PostConstruct
	public void init(){
		agentLogged = loginMB.getAgentLogged();
		if(agentLogged.isAdmin()){
			secretaries = sDao.findAllOrderedByName();
			insitutions = new DualListModel<Institution>(new ArrayList<Institution>(), new ArrayList<Institution>());
			departments = new DualListModel<Department>(new ArrayList<Department>(), new ArrayList<Department>());
			floors = new DualListModel<Floor>(new ArrayList<Floor>(), new ArrayList<Floor>());
			rooms = new DualListModel<Room>(new ArrayList<Room>(), new ArrayList<Room>());
			
		}else{
			WebTools.gotoHomeSystem();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onInstitutionTransfer(TransferEvent ev){
		secretary = sDao.loadSecretaryWithAllLazyLists(secretary.getId());
		List<Institution> insts = (List<Institution>)ev.getItems();
		FacesMessage fm = null;
		if(ev.isAdd()){
			//its add
			for (Institution institution : insts) {
				institution = iDao.loadInstitutionWithSecretaries(institution.getId());
				//Institution Side
				if(institution.getSecretaries() == null){
					List<Secretary> secs = new ArrayList<Secretary>();
					secs.add(secretary);
					institution.setSecretaries(secs);
					iDao.update(institution);
				}else{
					institution.getSecretaries().add(secretary);
					iDao.update(institution);
				}
				
				//Secretary Side
				if(secretary.getInstitutions() == null){
					List<Institution> ins = new ArrayList<Institution>();
					ins.add(institution);
					secretary.setInstitutions(ins);
					sDao.update(secretary);
				}else{
					secretary.getInstitutions().add(institution);
					sDao.update(secretary);
				}
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Direito adicionado com sucesso", "Sucesso");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}else{
			//is remove
			for (Institution institution : insts) {
				institution = iDao.loadInstitutionWithSecretaries(institution.getId());
				institution.getSecretaries().remove(secretary);
				iDao.update(institution);
				
				secretary.getInstitutions().remove(institution);
				sDao.update(secretary);
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Direito removido com sucesso", "Sucesso");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void onDepartmentTransfer(TransferEvent ev){
		secretary = sDao.loadSecretaryWithAllLazyLists(secretary.getId());
		List<Department> deps = (List<Department>)ev.getItems();
		FacesMessage fm = null;
		if(ev.isAdd()){
			//its add
			for (Department department : deps) {
				department = dDao.loadDepartmentWithSecretaries(department.getId());
				//Department Side
				if(department.getSecretaries() == null){
					List<Secretary> secs = new ArrayList<Secretary>();
					secs.add(secretary);
					department.setSecretaries(secs);
					dDao.update(department);
				}else{
					department.getSecretaries().add(secretary);
					dDao.update(department);
				}
				
				//Secretary Side
				if(secretary.getDepartments() == null){
					List<Department> ds = new ArrayList<Department>();
					ds.add(department);
					secretary.setDepartments(ds);
					sDao.update(secretary);
				}else{
					secretary.getDepartments().add(department);
					sDao.update(secretary);
				}
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Direito adicionado com sucesso", "Sucesso");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}else{
			//is remove
			for (Department department : deps) {
				department = dDao.loadDepartmentWithSecretaries(department.getId());
				department.getSecretaries().remove(secretary);
				dDao.update(department);
				
				secretary.getDepartments().remove(department);
				sDao.update(secretary);
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Direito removido com sucesso", "Sucesso");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onFloorTransfer(TransferEvent ev){
		secretary = sDao.loadSecretaryWithAllLazyLists(secretary.getId());
		List<Floor> fs = (List<Floor>)ev.getItems();
		FacesMessage fm = null;
		if(ev.isAdd()){
			//its add
			for (Floor floor : fs) {
				floor = fDao.loadFloortWithSecretaries(floor.getId());
				//Floor Side
				if(floor.getSecretaries() == null){
					List<Secretary> secs = new ArrayList<Secretary>();
					secs.add(secretary);
					floor.setSecretaries(secs);
					fDao.update(floor);
				}else{
					floor.getSecretaries().add(secretary);
					fDao.update(floor);
				}
				
				//Secretary Side
				if(secretary.getDepartments() == null){
					List<Floor> floors = new ArrayList<Floor>();
					floors.add(floor);
					secretary.setFloors(floors);
					sDao.update(secretary);
				}else{
					secretary.getFloors().add(floor);
					sDao.update(secretary);
				}
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Direito adicionado com sucesso", "Sucesso");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}else{
			//is remove
			for (Floor floor : fs) {
				floor = fDao.loadFloortWithSecretaries(floor.getId());
				floor.getSecretaries().remove(secretary);
				fDao.update(floor);
				
				secretary.getFloors().remove(floor);
				sDao.update(secretary);
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Direito removido com sucesso", "Sucesso");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onRoomTransfer(TransferEvent ev){
		secretary = sDao.loadSecretaryWithAllLazyLists(secretary.getId());
		List<Room> rs = (List<Room>)ev.getItems();
		FacesMessage fm = null;
		if(ev.isAdd()){
			//its add
			for (Room room : rs) {
				room = rDao.loadRoomWithSecretaries(room.getId());
				//
				//Room Side
				if(room.getSecretaries() == null){
					List<Secretary> secs = new ArrayList<Secretary>();
					secs.add(secretary);
					room.setSecretaries(secs);
					rDao.update(room);
				}else{
					room.getSecretaries().add(secretary);
					rDao.update(room);
				}
				
				//Secretary Side
				if(secretary.getRooms() == null){
					List<Room> rooms = new ArrayList<Room>();
					rooms.add(room);
					secretary.setRooms(rooms);
					sDao.update(secretary);
				}else{
					secretary.getRooms().add(room);
					sDao.update(secretary);
				}
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Direito adicionado com sucesso", "Sucesso");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}else{
			//is remove
			for (Room room : rs) {
				room = rDao.loadRoomWithSecretaries(room.getId());
				room.getSecretaries().remove(secretary);
				rDao.update(room);
				
				secretary.getRooms().remove(room);
				sDao.update(secretary);
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Direito removido com sucesso", "Sucesso");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}
	}
	
	public void updateBuildings(){
		if(secretary != null){
			List<Institution> sourceInst = iDao.getByAgentStrictly(agentLogged);
			List<Institution> targetInst = iDao.getByAgentStrictly(secretary);
			for (int i = 0; i < sourceInst.size(); i++) {
				for (int j = 0; j < targetInst.size(); j++) {
					if(sourceInst.get(i).getId().equals(targetInst.get(j).getId())){
						sourceInst.remove(i);
						i--;
						break;
					}
				}
			}
			insitutions = new DualListModel<Institution>(sourceInst, targetInst);
			
			List<Department> sourceDep = dDao.getByAgentStrictly(agentLogged);
			List<Department> targetDep = dDao.getByAgentStrictly(secretary);
			for (int i = 0; i < sourceDep.size(); i++) {
				for (int j = 0; j < targetDep.size(); j++) {
					if(sourceDep.get(i).getId().equals(targetDep.get(j).getId())){
						sourceDep.remove(i);
						i--;
						break;
					}
				}
			}
			departments = new DualListModel<Department>(sourceDep, targetDep);
			
			List<Floor> sourceFloor = fDao.getByAgentStrictly(agentLogged);
			List<Floor> targetFloor = fDao.getByAgentStrictly(secretary);
			for (int i = 0; i < sourceFloor.size(); i++) {
				for (int j = 0; j < targetFloor.size(); j++) {
					if(sourceFloor.get(i).getId().equals(targetFloor.get(j).getId())){
						sourceFloor.remove(i);
						i--;
						break;
					}
				}
			}
			floors = new DualListModel<Floor>(sourceFloor, targetFloor);
			
			List<Room> sourceRoom = rDao.getByAgentStrictly(agentLogged);
			List<Room> targetRoom = rDao.getByAgentStrictly(secretary);
			for (int i = 0; i < sourceRoom.size(); i++) {
				for (int j = 0; j < targetRoom.size(); j++) {
					if(sourceRoom.get(i).getId().equals(targetRoom.get(j).getId())){
						sourceRoom.remove(i);
						i--;
						break;
					}
				}
			}
			rooms = new DualListModel<Room>(sourceRoom, targetRoom);
		}
	}
	
	public List<Secretary> completeSecretaries(String query){
		List<Secretary> suggestions = new ArrayList<Secretary>();
		for (Secretary sec : secretaries) {
			if(sec.getFullname().toLowerCase().contains(query.toLowerCase()) || sec.getUsername().toLowerCase().contains(query.toLowerCase())){
				suggestions.add(sec);
			}
		}
		
		return suggestions;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public List<Secretary> getSecretaries() {
		return secretaries;
	}

	public void setSecretaries(List<Secretary> secretaries) {
		this.secretaries = secretaries;
	}

	public Secretary getSecretary() {
		return secretary;
	}

	public void setSecretary(Secretary secretary) {
		this.secretary = secretary;
	}

	public DualListModel<Institution> getInsitutions() {
		return insitutions;
	}

	public void setInsitutions(DualListModel<Institution> insitutions) {
		this.insitutions = insitutions;
	}

	public DualListModel<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(DualListModel<Department> departments) {
		this.departments = departments;
	}

	public DualListModel<Floor> getFloors() {
		return floors;
	}

	public void setFloors(DualListModel<Floor> floors) {
		this.floors = floors;
	}

	public DualListModel<Room> getRooms() {
		return rooms;
	}

	public void setRooms(DualListModel<Room> rooms) {
		this.rooms = rooms;
	}
	
	

}
