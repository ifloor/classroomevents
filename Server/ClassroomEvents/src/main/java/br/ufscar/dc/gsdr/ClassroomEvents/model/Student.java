package br.ufscar.dc.gsdr.ClassroomEvents.model;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

@Entity
@Inheritance
@DiscriminatorValue(value="4")
public class Student extends Agent{

}
