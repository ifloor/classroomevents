package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Floor;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;


@Stateless
@TransactionManagement
public class FloorDao extends HibernateEntityManagerGenericDao<Floor, Long>{

	public FloorDao() {
		super(Floor.class);
	}
	
	@EJB RoomDao rDao;
	@EJB SecretaryDao sDao;
	@EJB DepartmentDao dDao;
	
	@SuppressWarnings("unchecked")
	public List<Floor> findAllOrderedByName(){
		Criteria crit = getSession().createCriteria(Floor.class);
		crit.addOrder(Order.asc("name"));
		return crit.list();
	}
	
	public Floor loadFloortWithSecretaries(Long floorId){
		Criteria  crit = getSession().createCriteria(Floor.class);
		crit.add(Restrictions.eq("id", floorId));
		Floor floor = (Floor)crit.uniqueResult();
		if(floor != null){
			floor.getSecretaries().size();
		}
		return floor;
	} 
	
	@SuppressWarnings({ "unchecked"})
	public List<Floor> getByAgent(Agent agent){
		//Allow access to departments throught institutes highers
		if(agent instanceof Admin){
			return findAllOrderedByName();
		}
		Criteria crit = getSession().createCriteria(Floor.class);
		crit.createAlias("secretaries", "secs");
		crit.add(Restrictions.eq("secs.id", agent.getId()));
		crit.addOrder(Order.asc("name"));
		List<Floor> directly = crit.list();
		
		
		List<Department> dps = dDao.getByAgent(agent);
		if(dps== null || dps.size() == 0)
			return directly;
		crit = getSession().createCriteria(Floor.class);
		crit.add(Restrictions.in("department", dps));
		List<Floor> indirectly = crit.list();
		
		
		Set<Floor> s = new TreeSet<Floor>();
		s.addAll(directly);
		s.addAll(indirectly);
		return ((List<Floor>) (List<?>) Arrays.asList(s.toArray()));
		
	}
	
	/**
	 * Return ONLY the floors directly with access rights
	 * @param Agent 
	 * @return List<Floor>
	 */
	@SuppressWarnings("unchecked")
	public List<Floor> getByAgentStrictly(Agent agent){
		if(agent instanceof Admin){
			return findAllOrderedByName();
		}
		Criteria crit = getSession().createCriteria(Floor.class);
		crit.createAlias("secretaries", "secs");
		crit.add(Restrictions.eq("secs.id", agent.getId()));
		crit.addOrder(Order.asc("name"));
		return crit.list();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Floor> getFloors(Long depId){
		Criteria crit = getSession().createCriteria(Floor.class);
		crit.createAlias("department", "dep");
		crit.add(Restrictions.eq("dep.id", depId));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public Floor getRecent(String name, Long depId){
		try{Criteria crit = getSession().createCriteria(Floor.class);
		crit.createAlias("department", "dep");
		crit.add(Restrictions.eq("dep.id", depId));
		crit.add(Restrictions.eq("name", name));
		crit.addOrder(Order.desc("id"));
		return ((List<Floor>)crit.list()).get(0);
		}catch(Exception e){e.printStackTrace(); return null;}
	}
	
	@SuppressWarnings("unchecked")
	public void deleteWithRooms(Long floorId){
		List<Room> rooms = rDao.getByFloor(floorId);
		for (Room room : rooms) {
			rDao.deleteWithReseverAndResourceRoom(room.getId());
		}
		List<Secretary> secretaries = null;
		Criteria crit = getSession().createCriteria(Secretary.class);
		crit.createAlias("floors", "f");
		crit.add(Restrictions.eq("f.id", floorId));
		secretaries = crit.list();
		if(secretaries != null){
			for (Secretary secretary : secretaries) {
				for (int i = 0; i < secretary.getFloors().size(); i++) {
					secretary.getFloors().remove(i);
					i--;
				}
				sDao.updateFlushing(secretary);
			}
		}
		Floor f = findById(floorId);
		f.setSecretaries( null);
		update(f);
		getSession().flush();
		delete(floorId);
	}
}
