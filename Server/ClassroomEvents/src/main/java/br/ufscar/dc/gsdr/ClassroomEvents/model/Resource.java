package br.ufscar.dc.gsdr.ClassroomEvents.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class Resource {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private String company;
	
	@Column
	private String description;
	
	@Column
	private String model;
	
	@OneToMany(mappedBy="resource")
	@JsonIgnore
	private List<ResourceRoom> resourceRooms;
	
	public Resource(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public List<ResourceRoom> getResourceRooms() {
		return resourceRooms;
	}

	public void setResourceRooms(List<ResourceRoom> resourceRooms) {
		this.resourceRooms = resourceRooms;
	}

	public Long getId() {
		return id;
	}
	
	
}
