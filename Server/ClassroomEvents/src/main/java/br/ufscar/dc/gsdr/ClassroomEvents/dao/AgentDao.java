package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;

@Stateless
@TransactionManagement
public class AgentDao extends HibernateEntityManagerGenericDao<Agent, Long>{

	public AgentDao(){
		super(Agent.class);
	}
	
	@SuppressWarnings("unchecked")
	public Agent getByUsername(String username){
		try{
			Criteria crit = getSession().createCriteria(Agent.class);
			crit.add(Restrictions.eq("username", username));
			return (((List<Agent>)crit.list()).get(0));
		}
		catch(Exception e){return null;}
	}
	
	@SuppressWarnings("unchecked")
	public List<Agent> findAllOrderedByFullName(){
		Criteria crit = getSession().createCriteria(Agent.class);
		crit.addOrder(Order.asc("fullname"));
		return crit.list();
	}
}
