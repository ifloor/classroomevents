package br.ufscar.dc.gsdr.ClassroomEvents.Service;

import java.util.HashMap;
import java.util.Map;


public class TokenVault {
	Map<String, Token> tokenMap;
	String holdenEntity;
	
	public TokenVault(String holdenEntity){
		this.holdenEntity = holdenEntity;
		tokenMap = new HashMap<String, Token>();
	}
	
	public Token getToken(String token){
		return tokenMap.get(token);
	}
	
	public void removeToken(String token){
		tokenMap.remove(token);
	}
	
	public void putToken(Token t){
		tokenMap.put(t.getValue(), t);
	}
}
