package br.ufscar.dc.gsdr.ClassroomEvents.enumeration;

public enum Entities {
	ADMIN,SECRETARY,STUDENT,TEACHER;
	
	
	public String toStringSpecial(){
		switch (this) {
		case ADMIN:
			return "admin";
		case SECRETARY:
			return "secretary";
		case STUDENT:
			return "student";
		case TEACHER:
			return "teacher";
		default:
			return "";
		}
	}
	
	public String toStringSpecialPortuguese(){
		switch (this) {
		case ADMIN:
			return "Admin";
		case SECRETARY:
			return "Secretario(a)";
		case STUDENT:
			return "Estudante";
		case TEACHER:
			return "Professor";
		default:
			return "";
		}
	}
	
	public static Entities valueOfSpecial(String value){
		if(value.equals("admin"))
			return ADMIN;
		if(value.equals("secretary"))
			return SECRETARY;
		if(value.equals("student"))
			return STUDENT;
		if(value.equals("teacher"))
			return TEACHER;
		return null;
	}
}
