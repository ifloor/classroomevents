package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.InstitutionDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Institution;

@ManagedBean
@ViewScoped
public class InstitutionListMB implements Serializable{
	private static final long serialVersionUID = 5733680385801530200L;
	
	@EJB
	private InstitutionDao iDao;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	private Agent agent;
	
	private List<Institution> institutions,filteredInstitutions;

	private Institution institution;
	
	@PostConstruct
	public void init(){
		agent = loginMB.getAgentLogged();
		institutions = iDao.getByAgent(agent);
//		System.out.println("i[list] have been instatiated");
	}
	
	
	public void deleteInstitution(Institution ins){
		iDao.deleteWithDepartments(ins.getId());
		for (int i = 0; i < institutions.size(); i++) {
			if(ins.getId().equals(institutions.get(i).getId())){
				institutions.remove(i);
			}
		}
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Instituição deletada com sucesso", "Sucesso!");
		FacesContext.getCurrentInstance().addMessage(null, fm);
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public List<Institution> getInstitutions() {
		return institutions;
	}

	public void setInstitutions(List<Institution> institutions) {
		this.institutions = institutions;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public List<Institution> getFilteredInstitutions() {
		return filteredInstitutions;
	}

	public void setFilteredInstitutions(List<Institution> filteredInstitutions) {
		this.filteredInstitutions = filteredInstitutions;
	}
	
	
	
	
	
	
}
