package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.DepartmentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.FloorDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.RoomDao;
import br.ufscar.dc.gsdr.ClassroomEvents.enumeration.Building;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Floor;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;
import br.ufscar.dc.gsdr.ClassroomEvents.util.Cartesian2PointsWidhtHeightObject;
import br.ufscar.dc.gsdr.ClassroomEvents.util.Cartesian4PointsObject;
import br.ufscar.dc.gsdr.ClassroomEvents.util.CoordinatesConverter;

@ManagedBean
@ViewScoped
public class RoomListCreateMB implements Serializable{
	private static final long serialVersionUID = -1029512870160009053L;
	
	@EJB DepartmentDao dDao;
	@EJB FloorDao fDao;
	@EJB private RoomDao rDao;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	@ManagedProperty(value="#{cropperBean}")
	private CropperBean cropperBean;
	
	@ManagedProperty(value="#{imagesKeeperMB}")
	private ImagesKeeperMB imagesKeeper;
	
	private Department departmentSelected;
	private Floor floorSelected;
	private Room room;
	
	private List<Department> departments;
	
	private List<Floor> floors;
	private List<Room> rooms, roomsFiltered;
	
	private Agent agent;
	
	private String realPathToPhoto;
	
	
	
	public RoomListCreateMB(){
		
	}
	
	@PostConstruct
	public void init(){
		agent = loginMB.getAgentLogged();
		departments = dDao.getByAgent(agent);
		floors = new ArrayList<Floor>();
	}
	
	@PreDestroy
	public void destroy(){
		
	}
	
	public List<Department> completeDepartment(String query) {  
        List<Department> suggestions = new ArrayList<Department>();  
          
        for(Department dep : departments) {  
            if(dep.getName().toLowerCase().contains(query.toLowerCase()) || dep.getInstitution().getName().toLowerCase().contains(query.toLowerCase()))  
                suggestions.add(dep);  
        }  
          
        return suggestions;  
    }
	
	public List<Floor> completeFloor(String query) {  
        
		List<Floor> suggestions = new ArrayList<Floor>();
		if(floors != null){
	        for(Floor floor : floors) {  
	            if(floor.getName().toLowerCase().contains(query.toLowerCase()))  
	                suggestions.add(floor);  
	        }
		}
          
        return suggestions;  
    }
	
	public void updateFloors(){
		if(departmentSelected != null){
			floors = fDao.getFloors(departmentSelected.getId());
			if(floors == null)
				floors = new ArrayList<Floor>();
		}
	}
	
	public void updateRooms(){
		if(floorSelected != null){
			rooms = rDao.getByFloor(floorSelected.getId());
			if(rooms == null)
				rooms = new ArrayList<Room>();
			prepareRealPath();
		}
	}
	
	private void prepareRealPath(){
		realPathToPhoto =  imagesKeeper.getRegister(Building.FLOOR, floorSelected.getId());
	}
	
	public void addRoom(){
		if(room != null){
			Cartesian4PointsObject c4 = CoordinatesConverter.to4P(new Cartesian2PointsWidhtHeightObject(cropperBean.getImageCropped().getTop(), 
					cropperBean.getImageCropped().getLeft(), cropperBean.getImageCropped().getWidth(), 
					cropperBean.getImageCropped().getHeight()));
			room.setFloor(floorSelected);
			room.setXa(c4.getXa());
			room.setXb(c4.getXb());
			room.setYa(c4.getYa());
			room.setYb(c4.getYb());
			rDao.save(room);
			rooms.add(room);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sala cadastrada com sucesso", "Sucesso!");
			FacesContext.getCurrentInstance().addMessage(null, fm);
			room = null;
		}
	}
	
	public void prepareRoom(){
		room = new Room();
	}
	
	public void deleteFloor(Room r){
		rDao.deleteWithReseverAndResourceRoom(r.getId());
		for (int i = 0; i < rooms.size(); i++) {
			if(r.getId().equals(rooms.get(i).getId())){
				rooms.remove(i);
			}
		}
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sala deletada com sucesso", "Sucesso!");
		FacesContext.getCurrentInstance().addMessage(null, fm);
	}

	public Department getDepartmentSelected() {
		return departmentSelected;
	}

	public void setDepartmentSelected(Department departmentSelected) {
		this.departmentSelected = departmentSelected;
	}

	

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<Floor> getFloors() {
		return floors;
	}

	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public List<Room> getRoomsFiltered() {
		return roomsFiltered;
	}

	public void setRoomsFiltered(List<Room> roomsFiltered) {
		this.roomsFiltered = roomsFiltered;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public Floor getFloorSelected() {
		return floorSelected;
	}

	public void setFloorSelected(Floor floorSelected) {
		this.floorSelected = floorSelected;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public CropperBean getCropperBean() {
		return cropperBean;
	}

	public void setCropperBean(CropperBean cropperBean) {
		this.cropperBean = cropperBean;
	}

	public ImagesKeeperMB getImagesKeeper() {
		return imagesKeeper;
	}

	public void setImagesKeeper(ImagesKeeperMB imagesKeeper) {
		this.imagesKeeper = imagesKeeper;
	}

	public String getRealPathToPhoto() {
		return realPathToPhoto;
	}

	public void setRealPathToPhoto(String realPathToPhoto) {
		this.realPathToPhoto = realPathToPhoto;
	}
	
	
	

	
	
}
