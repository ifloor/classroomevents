package br.ufscar.dc.gsdr.ClassroomEvents.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.DepartmentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;
import br.ufscar.dc.gsdr.ClassroomEvents.util.Retangulo;

@Path("/department")
public class DepartmentService {

	
	@EJB
	DepartmentDao departmentDao;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDepartmentsByInstituitionId(@QueryParam("id") Long id){
		try{
			List<Department> deps = departmentDao.getDepartments(id);
			
			return Response.ok(deps).status(Status.OK).build();
		}catch(Exception e){
			return Response.ok(e.getMessage()).status(Status.OK).build();
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getDepartments(Retangulo rect){
		try{
			List<Department> deps = departmentDao.getDepartmentsIn(rect);
			
			return Response.ok(deps).status(Status.OK).build();
		}catch(Exception e){
			return Response.ok(e.getMessage()).status(Status.OK).build();
		}
	}
}
