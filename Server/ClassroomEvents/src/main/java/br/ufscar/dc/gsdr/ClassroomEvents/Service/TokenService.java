package br.ufscar.dc.gsdr.ClassroomEvents.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import br.ufscar.dc.gsdr.ClassroomEvents.enumeration.Entities;

public class TokenService {
	private static TokenService instance = null;
	private static final String ALPHABET="abcdefghijklmnopqrstuvxywzABCDEFGHIJKLMONPQRSTUVXYWZ0123456789";
	
	private Map<Entities, TokenVault> vaultsMap;
	
	
	private TokenService(){
		vaultsMap = new HashMap<Entities, TokenVault>();
	}

	public static TokenService getInstance() {
		if(instance == null)
			instance = new TokenService();
		return instance;
	}
	
	public Token createNewToken(String entity,String username){
		Entities entityEnum = null;
		entityEnum = Entities.valueOfSpecial(entity);
		if(entityEnum != null){
			Token t = new Token();
			t.setTTL(new Date(System.currentTimeMillis()+1000*60*60*24)); // 1dia
			t.setValue(generateRandomToken());
			t.setOwnerUsername(username);
			t.setType(entity);
			
			TokenVault vault = getVault(entityEnum);
			vault.putToken(t);
			
			return t;
		}
		
		
		
		return null;
	}
	
	public boolean hasToken(String entity, String username, String token){
		Entities entityEnum = null;
		TokenVault vault = null;
		Token t = null;
		
		entityEnum = Entities.valueOfSpecial(entity);
		if(entityEnum != null){
			vault = getVault(entityEnum);
			t = vault.getToken(token);
			if(t != null){
				if(t.getTTL().compareTo(new Date()) > 0){
					if(t.getOwnerUsername().contentEquals(username))
						return true;
					else
						return false;
				}else{
					vault.removeToken(token);
				}
			}
		}
		return false;
	}
	
	private TokenVault getVault(Entities entity){
		TokenVault vault = vaultsMap.get(entity);
		if(vault == null){
			vault = new TokenVault(entity.toStringSpecial());
			vaultsMap.put(entity, vault);
		}
		return vault;
	}
	
	private String generateRandomToken(){
		String newToken="";
		Random r = new Random();
		for (int i = 0; i < 128; i++) {
			newToken+=ALPHABET.charAt(r.nextInt(62));
		}
		return newToken;
	}
	
	
}
