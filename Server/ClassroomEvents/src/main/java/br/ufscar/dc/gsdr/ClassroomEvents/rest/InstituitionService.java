package br.ufscar.dc.gsdr.ClassroomEvents.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.InstitutionDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Institution;

@Path("/institution")
public class InstituitionService {

	@EJB
	private InstitutionDao institutionDao;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInstitution(@QueryParam("x") Double x, @QueryParam("y") Double y){
		try{
			if(x == null || y == null)
				Response.ok("Null parameters").status(Status.OK).build();
			
			List<Institution> inst = institutionDao.getInstitution(x,y);
			return Response.ok(inst).status(Status.OK).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.ok(e.getMessage()).status(Status.OK).build();
		}
	}
}
