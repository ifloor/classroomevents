package br.ufscar.dc.gsdr.ClassroomEvents.model;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Inheritance
@DiscriminatorValue(value="3")
public class Teacher extends Agent{
	
	
	
	public Teacher(){
		
	}
	
	@OneToMany(mappedBy="teacher")
	@JsonIgnore
	private List<Reserve> reserves;

	public List<Reserve> getReserves() {
		return reserves;
	}

	public void setReserves(List<Reserve> reserves) {
		this.reserves = reserves;
	}

	
	
	

	
	
	

}
