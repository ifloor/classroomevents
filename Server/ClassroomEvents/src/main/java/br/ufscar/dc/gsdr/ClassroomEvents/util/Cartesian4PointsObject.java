package br.ufscar.dc.gsdr.ClassroomEvents.util;

public class Cartesian4PointsObject {
	private Double xa,xb,ya,yb;
	
	public Cartesian4PointsObject(){
		
	}

	public Cartesian4PointsObject(Double xa, Double xb, Double ya, Double yb) {
		super();
		this.xa = xa;
		this.xb = xb;
		this.ya = ya;
		this.yb = yb;
	}

	public Double getXa() {
		return xa;
	}

	public void setXa(Double xa) {
		this.xa = xa;
	}

	public Double getXb() {
		return xb;
	}

	public void setXb(Double xb) {
		this.xb = xb;
	}

	public Double getYa() {
		return ya;
	}

	public void setYa(Double ya) {
		this.ya = ya;
	}

	public Double getYb() {
		return yb;
	}

	public void setYb(Double yb) {
		this.yb = yb;
	}
	
	
	
}
