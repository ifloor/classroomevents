package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.DepartmentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.FloorDao;
import br.ufscar.dc.gsdr.ClassroomEvents.enumeration.Building;
import br.ufscar.dc.gsdr.ClassroomEvents.images.ImageDaemon;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Floor;

@ManagedBean
@ViewScoped
public class FloorListCreateMB implements Serializable{
	private static final long serialVersionUID = -1029512870160009053L;
	
	@EJB DepartmentDao dDao;
	@EJB FloorDao fDao;
	@EJB private ImageDaemon imgService;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	private Department departmentSelected;
	private Floor floor;
	
	private List<Department> departments;
	
	private List<Floor> floors,floorsFiltered;
	
	private Agent agent;
	
	private UploadedFile file;
	
	public FloorListCreateMB(){
		
	}
	
	@PostConstruct
	public void init(){
		agent = loginMB.getAgentLogged();
		departments = dDao.getByAgent(agent);
		floors = new ArrayList<Floor>();
	}
	
	public List<Department> completeDepartment(String query) {  
        List<Department> suggestions = new ArrayList<Department>();  
          
        for(Department dep : departments) {  
            if(dep.getName().toLowerCase().contains(query.toLowerCase()) || dep.getInstitution().getName().toLowerCase().contains(query.toLowerCase()))  
                suggestions.add(dep);  
        }  
          
        return suggestions;  
    }
	
	public void updateFloors(){
		if(departmentSelected != null){
			floors = fDao.getFloors(departmentSelected.getId());
			if(floors == null)
				floors = new ArrayList<Floor>();
		}
	}
	
	public void addFloor(){
		if(floor != null){
			if(file != null){
				floor.setDepartment(departmentSelected);
				fDao.save(floor);
				floor = fDao.getRecent(floor.getName(), floor.getDepartment().getId());
				floors.add(floor);
				imgService.saveImage(file, Building.FLOOR, floor.getId());
				floor = null;
				file = null;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Andar cadastrado com sucesso", "Sucesso!");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}else{
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Selecione uma imagem para o andar!", "Error!");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}
	}
	
	public void prepareFloor(){
		floor = new Floor();
	}
	
	public void deleteFloor(Floor f){
		fDao.deleteWithRooms(f.getId());
		for (int i = 0; i < floors.size(); i++) {
			if(f.getId().equals(floors.get(i).getId())){
				floors.remove(i);
			}
		}
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Andar deletado com sucesso", "Sucesso!");
		FacesContext.getCurrentInstance().addMessage(null, fm);
	}
	
	 public void handleFileUpload(FileUploadEvent event) {  
		 file =  event.getFile();
//	        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");  
//	        FacesContext.getCurrentInstance().addMessage(null, msg);  
	 }

	public Department getDepartmentSelected() {
		return departmentSelected;
	}

	public void setDepartmentSelected(Department departmentSelected) {
		this.departmentSelected = departmentSelected;
	}

	public Floor getFloor() {
		return floor;
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<Floor> getFloors() {
		return floors;
	}

	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public List<Floor> getFloorsFiltered() {
		return floorsFiltered;
	}

	public void setFloorsFiltered(List<Floor> floorsFiltered) {
		this.floorsFiltered = floorsFiltered;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}
	
	

	
	
}
