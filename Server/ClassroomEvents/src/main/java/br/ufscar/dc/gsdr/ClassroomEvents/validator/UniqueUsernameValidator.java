package br.ufscar.dc.gsdr.ClassroomEvents.validator;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.AgentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;


@ManagedBean
@RequestScoped
public class UniqueUsernameValidator implements Serializable{
	
	private static final long serialVersionUID = 4177223341827723929L;
	@EJB AgentDao aDao;

	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		System.out.println("validating...");
		Agent a = aDao.getByUsername(value.toString());
		//Implementando o Validator pra validar se um username é unico na hora de criar um usuário
		
		if(a == null){
			//System.out.println("new");
		}
		else{
			//System.out.println("used");
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Este username já está em uso", "Erro"));
		}
		
	}
}
