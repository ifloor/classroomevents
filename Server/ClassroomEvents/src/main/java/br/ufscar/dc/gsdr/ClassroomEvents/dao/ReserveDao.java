package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Reserve;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Teacher;

@Stateless
@TransactionManagement
public class ReserveDao extends HibernateEntityManagerGenericDao<Reserve, Long>{
	
	@EJB private RoomDao rDao;
	
	@SuppressWarnings("unchecked")
	public List<Reserve> getByRoomId(Long roomId){
		Criteria crit = getSession().createCriteria(Reserve.class);
		crit.createAlias("room", "r");
		crit.add(Restrictions.eq("r.id", roomId));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Reserve> getByRoomIdToday(Long roomId){
		Criteria crit = getSession().createCriteria(Reserve.class);
		crit.createAlias("room", "r");
		crit.add(Restrictions.eq("r.id", roomId));
		GregorianCalendar calIni =  new GregorianCalendar();
		calIni.set(Calendar.HOUR_OF_DAY, 0);
		calIni.set(Calendar.MINUTE, 0);
		calIni.set(Calendar.SECOND, 0);
		calIni.set(Calendar.MILLISECOND, 0);
		GregorianCalendar calFim =  new GregorianCalendar();
		calFim.set(Calendar.HOUR_OF_DAY, 0);
		calFim.set(Calendar.MINUTE, 0);
		calFim.set(Calendar.SECOND, 0);
		calFim.set(Calendar.MILLISECOND, 0);
		calFim.add(Calendar.DAY_OF_MONTH, 1);
		crit.add(Restrictions.between("beginMillis", calIni.getTime().getTime(), calFim.getTime().getTime()));
		crit.add(Restrictions.eq("reserveGranted", true));
		return crit.list();
	}
	
	public ReserveDao() {
		super(Reserve.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Reserve> getPendents(Agent agent){
		Criteria crit = getSession().createCriteria(Reserve.class);
		crit.add(Restrictions.isNull("reserveGranted"));
		List<Room> rooms = rDao.getByAgent(agent);
		if(rooms== null || rooms.size() == 0)
			return new ArrayList<Reserve>();
		crit.add(Restrictions.in("room", rooms));
		crit.addOrder(Order.asc("beginMillis"));
		crit.addOrder(Order.asc("requestedIn"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Reserve> getPendentsForTeachers(Teacher teacher){
		Criteria crit = getSession().createCriteria(Reserve.class);
		crit.add(Restrictions.isNull("reserveGranted"));
		crit.createAlias("teacher", "teacher");
		crit.add(Restrictions.eq("teacher.id", teacher.getId()));
		crit.addOrder(Order.asc("beginMillis"));
		crit.addOrder(Order.asc("requestedIn"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Reserve> getHistorical(Agent agent){
		Criteria crit = getSession().createCriteria(Reserve.class);
		crit.add(Restrictions.isNotNull("reserveGranted"));
		List<Room> rooms = rDao.getByAgent(agent);
		if(rooms== null || rooms.size() == 0)
			return new ArrayList<Reserve>();
		crit.add(Restrictions.in("room", rooms));
		crit.addOrder(Order.desc("requestedIn"));
		return crit.list();
	}
	
	public boolean isAvailable(Long roomId, Long start, Long end){
		Criteria crit = getSession().createCriteria(Reserve.class);
		
		crit.add(Restrictions.eq("reserveGranted", true));
		crit.createAlias("room", "room");
		crit.add(Restrictions.eq("room.id", roomId));
		
		
		crit.add(
			Restrictions.disjunction().add(
				Restrictions.conjunction().add(
					Restrictions.le("beginMillis", start)
				).add(
					Restrictions.ge("endMillis", start)
				)
			).add(
				Restrictions.conjunction().add(
					Restrictions.le("beginMillis", end)
				).add(
					Restrictions.ge("endMillis", end)
				)
			).add(
				Restrictions.conjunction().add(
					Restrictions.ge("beginMillis", start)
				).add(
					Restrictions.le("beginMillis", end)
				)
			)
		);
		return (crit.list().size() == 0);
	}

	public Long getReserve(Long reserveId, Long secretaryId) {
		
		
		String q = "select reserve.id from institution, department, floor, room, reserve"
				 +		" where"
				 +			"     reserve.id = " + reserveId
				 +			" and institution.id = department.institution_id"
				 +			" and department.id = floor.department_id"
				 +			" and floor.id = room.floor_id"
				 +			" and room.id = reserve.room_id"
				 +			" and ((institution.id in (select institutions_id from agent_institution where agent_id = " + secretaryId + "))"
				 +				" or (department.id in (select departments_id from agent_department where agent_id = " + secretaryId + "))"
				 +				" or (floor.id in (select floors_id from agent_floor where agent_id = " + secretaryId + "))"
				 +				" or (room.id in (select rooms_id from agent_room where agent_id = " + secretaryId + ")));";
		
		SQLQuery query = getSession().createSQLQuery(q);
		BigInteger r = (BigInteger)query.uniqueResult();
		if(r != null)
			return Long.parseLong(r.toString());
		return null;
	}
}
