package br.ufscar.dc.gsdr.ClassroomEvents.model;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.ManyToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@DiscriminatorValue(value="2")
@Inheritance
public class Secretary extends Agent{
		
	public Secretary(){
		
	}

	@ManyToMany
	@JsonIgnore
	private List<Institution> institutions;
	
	@ManyToMany
	@JsonIgnore
	private List<Department> departments;
	
	@ManyToMany
	@JsonIgnore
	private List<Floor> floors;
	
	@ManyToMany
	@JsonIgnore
	private List<Room> rooms;

	public List<Institution> getInstitutions() {
		return institutions;
	}

	public void setInstitutions(List<Institution> institutions) {
		this.institutions = institutions;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<Floor> getFloors() {
		return floors;
	}

	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}
	
	
	
}
