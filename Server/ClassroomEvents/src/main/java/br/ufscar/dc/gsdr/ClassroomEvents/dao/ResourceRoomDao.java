package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.ResourceRoom;

@Stateless
@TransactionManagement
public class ResourceRoomDao extends HibernateEntityManagerGenericDao<ResourceRoom, Long>{

	public ResourceRoomDao(){
		super(ResourceRoom.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<ResourceRoom> getByRoom(Long roomId){
		Criteria crit = getSession().createCriteria(ResourceRoom.class);
		crit.createAlias("room", "r");
		crit.add(Restrictions.eq("id", roomId));
		return crit.list();
	}
}
