package br.ufscar.dc.gsdr.ClassroomEvents.Service;

import java.util.Date;

public class Token {

	private String ownerUsername;
	
	private String value;
	
	private Date TTL;
	
	private String fullName;
	
	private String type;
	
	public Token(){
		
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getTTL() {
		return TTL;
	}

	public void setTTL(Date tTL) {
		TTL = tTL;
	}

	public String getOwnerUsername() {
		return ownerUsername;
	}

	public void setOwnerUsername(String ownerUsername) {
		this.ownerUsername = ownerUsername;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
