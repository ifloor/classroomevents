package br.ufscar.dc.gsdr.ClassroomEvents.model;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Reserve {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private Long beginMillis;
	
	@Column
	private Long endMillis;
	
	@Column
	private String teacherName;
	
	@Column
	private Date requestedIn;
	
	@Column
	private Boolean reserveGranted;
	
	@Column
	private Long userReserved;
	
	@ManyToOne
	private Room room;
	
	@ManyToOne
	private Teacher teacher;
	
	public Reserve(){
		
	}
	
	public String getTeacherNameProccessed(){
		if(teacher != null)
			return teacher.getFullname();
		else
			return teacherName;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reserve other = (Reserve) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public String reserveGrantedAsString(){
		if(reserveGranted != null){
			if(reserveGranted)
				return "Sim";
			else
				return "Não";
		}else{
			return "Não decidido";
		}
	}
	
	public Date getBeginMillisAsDate(){
		return new Date(beginMillis);
	}
	
	public Date getEndMillisAsDate(){
		return new Date(endMillis);
	}

	public Long getBeginMillis() {
		return beginMillis;
	}

	public void setBeginMillis(Long beginMillis) {
		this.beginMillis = beginMillis;
	}

	public Long getEndMillis() {
		return endMillis;
	}

	public void setEndMillis(Long endMillis) {
		this.endMillis = endMillis;
	}

	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public Date getRequestedIn() {
		return requestedIn;
	}
	public void setRequestedIn(Date requestedIn) {
		this.requestedIn = requestedIn;
	}
	public Boolean getReserveGranted() {
		return reserveGranted;
	}
	public void setReserveGranted(Boolean reserveGranted) {
		this.reserveGranted = reserveGranted;
	}
	public Room getRoom() {
		return room;
	}
	public void setRoom(Room room) {
		this.room = room;
	}
	public Long getId() {
		return id;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Long getUserReserved() {
		return userReserved;
	}

	public void setUserReserved(Long userReserved) {
		this.userReserved = userReserved;
	}
	
	
	
	
	
	
}
