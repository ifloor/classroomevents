package br.ufscar.dc.gsdr.ClassroomEvents.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.ufscar.dc.gsdr.ClassroomEvents.managedBean.LoginMB;

@WebFilter(urlPatterns="/management/*")
public class AgentFilter implements Filter{

	public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpSession session = req.getSession(false);
		
		if(session == null){
			//session null
			HttpServletResponse resp = (HttpServletResponse) response;
			resp.sendRedirect(req.getContextPath()+"/login.xhtml");
		}else{
			//session not null
			LoginMB loginMB = (LoginMB)session.getAttribute("loginMB");
			if(loginMB == null){
				//LoginMB is null;
				HttpServletResponse resp = (HttpServletResponse) response;
				resp.sendRedirect(req.getContextPath()+"/login.xhtml");
			}else{
				//LoginMB isnot null
				if(loginMB.getLogged()){
					//user is logged
					chain.doFilter(request, response);
				}else{
					//user isnot logged
					HttpServletResponse resp = (HttpServletResponse) response;
					resp.sendRedirect(req.getContextPath()+"/login.xhtml");
				}
			}
		}
		
		
        //Get the IP address of client machine.
        //String ipAddress = request.getRemoteAddr();
         
        //Log the IP address and current timestamp.
//        System.out.println("IP "+ipAddress + ", Time "
//                            + new Date().toString());
        
    }
    public void init(FilterConfig config) throws ServletException {
         
    }
    public void destroy() {
        //add code to release any resource
    }
	
}
