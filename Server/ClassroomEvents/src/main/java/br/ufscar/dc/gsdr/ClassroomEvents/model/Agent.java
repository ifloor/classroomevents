package br.ufscar.dc.gsdr.ClassroomEvents.model;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.bouncycastle.util.encoders.Hex;
import org.codehaus.jackson.annotate.JsonIgnore;

import br.ufscar.dc.gsdr.ClassroomEvents.enumeration.Entities;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="user_type",discriminatorType=DiscriminatorType.INTEGER)
public abstract class Agent {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String fullname;
	
	@Column
	private String username;
	
	@Column
	@JsonIgnore
	private String password;
	
	@Column
	@JsonIgnore
	private String token;
	
	@Column
	@JsonIgnore
	private Date tokenTTL;
	
	public Agent(){
		
	}
	
	public boolean isAdmin(){
		if (this instanceof Admin)
			return true;
		else
			return false;
	}
	
	public boolean isSecretary(){
		if (this instanceof Secretary)
			return true;
		else
			return false;
	}
	
	public boolean isTeacher(){
		if (this instanceof Teacher)
			return true;
		else
			return false;
	}
	
	public String getPresentableInstanceName(){
		if(this instanceof Admin){
			return "Admin";
		}else{
			if(this instanceof Student){
				return "Estudante";
			}else{
				if(this instanceof Teacher){
					return "Professor";
				}else{
					if(this instanceof Secretary){
						return "Secretário";
					}else{
						return "";
					}
				}
			}
		}
	}
	
	public static Agent instantiateNew(Entities entity){
		switch (entity) {
		case ADMIN:
			return new Admin();
		case SECRETARY:
			return new Secretary();
		case STUDENT:
			return new Student();
		case TEACHER:
			return new Teacher();
		default:
			return null;
		}
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agent other = (Agent) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}
	
	public static String hashPassword(String password){
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.reset();
			messageDigest.update(password.getBytes(Charset.forName("UTF8")));
			return new String(Hex.encode(messageDigest.digest()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch(NullPointerException npe){
			
		}
		return "";
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getTokenTTL() {
		return tokenTTL;
	}

	public void setTokenTTL(Date tokenTTL) {
		this.tokenTTL = tokenTTL;
	}
	
	
	
//	public static void main(String[] args) {
//		System.out.println("molina" + hashPassword("molina"));
//	}
	
	
}
