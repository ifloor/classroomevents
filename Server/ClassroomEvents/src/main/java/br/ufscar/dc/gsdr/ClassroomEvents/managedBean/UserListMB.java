package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.AgentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.util.WebTools;

@ManagedBean
@ViewScoped
public class UserListMB implements Serializable{
	private static final long serialVersionUID = 4921384266630165144L;
	
	@EJB private AgentDao aDao;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	private Agent agentLogged;
	
	private List<Agent> agents,filteredAgents;
	
	public UserListMB(){
		
	}
	
	@PostConstruct
	public void init(){
		agentLogged = loginMB.getAgentLogged();
		if(!(agentLogged instanceof Admin))
			WebTools.gotoHomeSystem();
		agents = aDao.findAllOrderedByFullName();
	}

	public Agent getAgentLogged() {
		return agentLogged;
	}

	public void setAgentLogged(Agent agentLogged) {
		this.agentLogged = agentLogged;
	}

	public List<Agent> getAgents() {
		return agents;
	}

	public void setAgents(List<Agent> agents) {
		this.agents = agents;
	}

	public List<Agent> getFilteredAgents() {
		return filteredAgents;
	}

	public void setFilteredAgents(List<Agent> filteredAgents) {
		this.filteredAgents = filteredAgents;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}
	
	
	

}
