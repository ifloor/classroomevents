package br.ufscar.dc.gsdr.ClassroomEvents.util;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;

/**
 * Classe com métodos utilitários a serem usados no desenvolvimento usando jsf
 *
 * @author      Alvaro Viebrantz
 * @version     1.0
 * @since       21/05/2012
 */
public class WebTools {

	/**
	* Função de redirecionamento
	* 
	* @param page Página que deseja fazer o redirecionamento
	* @author Alvaro Viebrantz
	*/
	public static void gotoPage(String page) {

        FacesContext fc = FacesContext.getCurrentInstance();
        NavigationHandler nav = fc.getApplication().getNavigationHandler();
        
        if(! page.startsWith("/")){
        	page = "/"+page;
        }
        
        if(page.contains("?")){
        	nav.handleNavigation(fc, null,page + "&faces-redirect=true");
        }else{
        	nav.handleNavigation(fc, null,page + "?faces-redirect=true");
        }
        fc.renderResponse();
    }
	
	
	public static void gotoHome(){
		gotoPage("index.xhtml");
	}
	
	public static void gotoHomeSystem(){
		gotoPage("management/home.xhtml");
	}
}
