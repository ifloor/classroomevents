package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.InstitutionDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Institution;
import br.ufscar.dc.gsdr.ClassroomEvents.util.WebTools;

@ManagedBean
@ViewScoped
public class InstituionCreateMB implements Serializable{
	private static final long serialVersionUID = 5733680385801530200L;
	
	@EJB
	private InstitutionDao iDao;
	
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	@ManagedProperty(value="#{mapBean}")
	private MapBean mapBean;
	
	private Agent agent;
	

	private Institution institution;
	
	@PostConstruct
	public void init(){
		agent = loginMB.getAgentLogged();
		if(! agent.isAdmin())
			WebTools.gotoPage("/management/institution/list.xhtml");
		institution = new Institution();
	}
	
	public String save(){
		institution.setXa(mapBean.getXa());
		institution.setXb(mapBean.getXb());
		institution.setYa(mapBean.getYa());
		institution.setYb(mapBean.getYb());
		iDao.save(institution);
		return "institutionList";
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public MapBean getMapBean() {
		return mapBean;
	}

	public void setMapBean(MapBean mapBean) {
		this.mapBean = mapBean;
	}

	
	

	
	
}
