package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Floor;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Reserve;
import br.ufscar.dc.gsdr.ClassroomEvents.model.ResourceRoom;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;


@Stateless
@TransactionManagement
public class RoomDao extends HibernateEntityManagerGenericDao<Room, Long>{

	@EJB private ResourceRoomDao rrDao;
	@EJB private ReserveDao reDao;
	@EJB private SecretaryDao sDao;
	@EJB private FloorDao fDao;
	
	public RoomDao(){
		super(Room.class);
	}
	
	public Room loadRoomWithSecretaries(Long roomId){
		Criteria  crit = getSession().createCriteria(Room.class);
		crit.add(Restrictions.eq("id", roomId));
		Room room = (Room)crit.uniqueResult();
		if(room != null){
			room.getSecretaries().size();
		}
		return room;
	} 
	
	public boolean isAvailable(Long roomId, Long start, Long end){
		Criteria crit = getSession().createCriteria(Reserve.class);
		
		crit.createAlias("room", "room");
		crit.add(Restrictions.eq("room.id", roomId));
		crit.add(Restrictions.eq("reserveGranted", true));
		
		crit.add(
			Restrictions.disjunction().add(
				Restrictions.conjunction().add(
					Restrictions.le("beginMillis", start)
				).add(
					Restrictions.ge("endMillis", start)
				)
			).add(
				Restrictions.conjunction().add(
					Restrictions.le("beginMillis", end)
				).add(
					Restrictions.ge("endMillis", end)
				)
			).add(
				Restrictions.conjunction().add(
					Restrictions.ge("beginMillis", start)
				).add(
					Restrictions.le("beginMillis", end)
				)
			)
		);
		
		
		return (crit.list().size() == 0);
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Room> getByFloor(Long floorId){
		Criteria crit = getSession().createCriteria(Room.class);
		crit.createAlias("floor", "fl");
		crit.add(Restrictions.eq("fl.id", floorId));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public void deleteWithReseverAndResourceRoom(Long roomId){
		//Reserve
		//ResourceRoom
		//Secretaries
		List<ResourceRoom> rrs = rrDao.getByRoom(roomId);
		for (ResourceRoom resourceRoom : rrs) {
			rrDao.delete(resourceRoom.getId());
		}
		List<Reserve> reserves = reDao.getByRoomId(roomId);
		for (Reserve reserve : reserves) {
			reDao.delete(reserve.getId());
		}
		
		List<Secretary> secretaries = null;
		Criteria crit = getSession().createCriteria(Secretary.class);
		crit.createAlias("rooms", "r");
		crit.add(Restrictions.eq("r.id", roomId));
		secretaries = crit.list();
		if(secretaries != null){
			for (Secretary secretary : secretaries) {
				for (int i = 0; i < secretary.getRooms().size(); i++) {
					secretary.getRooms().remove(i);
					i--;
				}
				sDao.updateFlushing(secretary);
			}
		}
		Room r = findById(roomId);
		r.setSecretaries( null);
		update(r);
		getSession().flush();
		delete(roomId);
	}
	
	@SuppressWarnings("unchecked")
	public List<Room> getByAgent(Agent agent){
		//TODO query to allow access throught higher inst, deps, floors,
		if(agent instanceof Admin)
			return findAll();
		Criteria crit = getSession().createCriteria(Room.class);
		crit.createAlias("secretaries", "secs");
		crit.add(Restrictions.eq("secs.id", agent.getId()));
		crit.addOrder(Order.asc("name"));
		List<Room> directly = crit.list();
		
		List<Floor> fs = fDao.getByAgent(agent);
		if(fs== null || fs.size() == 0)
			return directly;
		
		crit = getSession().createCriteria(Room.class);
		crit.add(Restrictions.in("floor", fs));
		List<Room> indirectly = crit.list();

		Set<Room> s = new TreeSet<Room>();
		s.addAll(directly);
		s.addAll(indirectly);
		return ((List<Room>) (List<?>) Arrays.asList(s.toArray()));
	}
	
	/**
	 * Return only the room with directly access right
	 * @param agent
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Room> getByAgentStrictly(Agent agent){
		if(agent instanceof Admin)
			return findAll();
		Criteria crit = getSession().createCriteria(Room.class);
		crit.createAlias("secretaries", "secs");
		crit.add(Restrictions.eq("secs.id", agent.getId()));
		crit.addOrder(Order.asc("name"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Room> getFloors(Long floorId){
		Criteria crit = getSession().createCriteria(Room.class);
		
		crit.createAlias("floor", "fl");
		crit.add(Restrictions.eq("fl.id", floorId));
		
		return crit.list();
		
	}
}
