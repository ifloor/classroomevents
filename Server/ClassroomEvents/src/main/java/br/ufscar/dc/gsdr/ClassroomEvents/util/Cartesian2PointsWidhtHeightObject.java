package br.ufscar.dc.gsdr.ClassroomEvents.util;

public class Cartesian2PointsWidhtHeightObject {
	private Long top,left,width,height;
	
	public Cartesian2PointsWidhtHeightObject(){
		
	}

	public Cartesian2PointsWidhtHeightObject(Long top, Long left, Long width,
			Long height) {
		super();
		this.top = top;
		this.left = left;
		this.width = width;
		this.height = height;
	}
	
	public Cartesian2PointsWidhtHeightObject(int top, int left, int width,
			int height) {
		super();
		this.top = new Long(top);
		this.left = new Long(left);
		this.width = new Long(width);
		this.height = new Long(height);
	}

	public Long getTop() {
		return top;
	}

	public void setTop(Long top) {
		this.top = top;
	}

	public Long getLeft() {
		return left;
	}

	public void setLeft(Long left) {
		this.left = left;
	}

	public Long getWidth() {
		return width;
	}

	public void setWidth(Long width) {
		this.width = width;
	}

	public Long getHeight() {
		return height;
	}

	public void setHeight(Long height) {
		this.height = height;
	}
	
	
}
