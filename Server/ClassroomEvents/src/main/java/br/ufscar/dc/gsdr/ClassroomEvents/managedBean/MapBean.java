package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.StateChangeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.LatLngBounds;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.primefaces.model.map.Rectangle;

@ManagedBean
@ViewScoped
public class MapBean implements Serializable{

	private static final long serialVersionUID = 5733680385801530200L;

	private MapModel model = new DefaultMapModel();
	
	Marker A, B;
	
	private String id01 = "A";
	private String id02 = "B";
	
	private LatLng center = new LatLng(-21.97978, -47.88054);
	private int zoom = 15;
	
	public MapBean() {
		setPosition(new LatLng(-21.97978, -47.88054));
	}
	
	public MapModel getModel() {
		return this.model;
	}

	
	public void onMarkerDrag(MarkerDragEvent  event){
		
		Marker draggedMarker = (Marker) event.getMarker();
		double neX, neY, swX, swY;
		
		if(draggedMarker.getTitle().equals(A.getTitle())){
			A = draggedMarker;
		}else if(draggedMarker.getTitle().equals(B.getTitle())){
			B = draggedMarker;
		}
		
		swX = A.getLatlng().getLng();
		swY = A.getLatlng().getLat();
		
		neX = B.getLatlng().getLng();
		neY = B.getLatlng().getLat();
		
		if(neX > swX){
			double aux = neX;
			neX = swX;
			swX = aux;
		}
		
		if(neY > swY){
			double aux = neY;
			neY = swY;
			swY = aux;
		}
		
		model.getRectangles().get(0).setBounds(new LatLngBounds(new LatLng(neY,neX), new LatLng(swY,swX)));
	}
	
	public void onPointSelect(PointSelectEvent event){
		setPosition(event.getLatLng());
	}
	
	
	private void setPosition(LatLng ll){

		model.getRectangles().clear();
		model.getMarkers().clear();
		
		LatLng coord1 = new LatLng(ll.getLat() - 0.0001, ll.getLng() - 0.0001);
		LatLng coord2 = new LatLng(ll.getLat() + 0.0001, ll.getLng() + 0.0001);
		
		A = new Marker(coord1, id01);
		B = new Marker(coord2, id02);
		
		LatLngBounds llb = new LatLngBounds(coord1, coord2);
		Rectangle rectangle = new Rectangle(llb);
		
		rectangle.setFillColor("#00CCFF");
		rectangle.setFillOpacity(0.25);
		rectangle.setStrokeColor("#0099FF");
		
		
		A.setDraggable(true);
		B.setDraggable(true);
		
		model.addOverlay(rectangle);
		model.addOverlay(A);
		model.addOverlay(B);
	}
	
	public void onStateChange(StateChangeEvent event){
		zoom = event.getZoomLevel();
		center = event.getCenter();
	}
	
	public String getCenter(){
		if(center != null)
			return (center.getLat() + ", " + center.getLng());
		
		return "";
	}
	
	public Double getXa(){
		return model.getRectangles().get(0).getBounds().getSouthWest().getLng();
	}
	
	public Double getXb(){
		return model.getRectangles().get(0).getBounds().getNorthEast().getLng();
	}
	
	public Double getYa(){
		return model.getRectangles().get(0).getBounds().getSouthWest().getLat();
	}
	
	public Double getYb(){
		return model.getRectangles().get(0).getBounds().getNorthEast().getLat();
	}

	public String getZoom(){
		return "" + zoom;
	}
	
	
}
