package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Student;

@Stateless
@TransactionManagement
public class StudentDao extends HibernateEntityManagerGenericDao<Student, Long>{
	
	public StudentDao(){
		super(Student.class);
	}
	
	
	public Student findByUsername(String username){
		Criteria crit = getSession().createCriteria(Student.class);
		crit.add(Restrictions.eq("username", username));
		Object obj = crit.uniqueResult();
		if(obj != null){
			return (Student)obj;
		}else{
			return null;
		}
	}

}
