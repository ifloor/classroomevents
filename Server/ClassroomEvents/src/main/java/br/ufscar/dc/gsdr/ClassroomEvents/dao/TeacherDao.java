package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Teacher;

@Stateless
@TransactionManagement
public class TeacherDao extends HibernateEntityManagerGenericDao<Teacher, Long>{
	
	public TeacherDao(){
		super(Teacher.class);
	}

	public Teacher findByUsername(String username){
		Criteria crit = getSession().createCriteria(Teacher.class);
		crit.add(Restrictions.eq("username", username));
		Object obj = crit.uniqueResult();
		if(obj != null){
			return (Teacher)obj;
		}else{
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Teacher> findAllOrderedByName(){
		Criteria crit = getSession().createCriteria(Teacher.class);
		crit.addOrder(Order.asc("fullname"));
		return crit.list();
	}
	
}
