package br.ufscar.dc.gsdr.ClassroomEvents.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.AgentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.ReserveDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.RoomDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.TeacherDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Reserve;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Teacher;

@Path("/reserve")
public class ReserveService {

	@EJB
	ReserveDao reserveDao;

	@EJB
	RoomDao roomDao;

	@EJB
	AgentDao agentDao;

	@EJB
	TeacherDao teacherDao;

	private static final String OK = "ok";
	private static final String MISSING_FIELDS = "Some fields is missing!";
	private static final String NOT_AVAILABLE = "This room os not available in this period!";
	private static final String UNAUTHORIZED = "Unauthorized action!";
	private static final String NOT_TEACHER = "Agent selected is not a Teacher!";
	private static final String ROOM_DOESNT_EXIST = "This room does not exist!";

	private class ValidatorResponse {
		String message;
		Agent requester;
		Teacher teacher;
		Reserve reserve;

		public ValidatorResponse() {
			message = null;
			requester = null;
			teacher = null;
			reserve = null;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public Agent getRequester() {
			return requester;
		}

		public void setRequester(Agent requester) {
			this.requester = requester;
			if (reserve != null) {
				reserve.setUserReserved(this.requester.getId());
			}
		}

		public void setTeacher(Teacher teacher) {

			this.teacher = teacher;
			if (reserve != null) {
				reserve.setTeacher(this.teacher);
				reserve.setTeacherName(this.teacher.getFullname());
			}
		}

		public Reserve getReserve() {
			return reserve;
		}

		public void setReserve(Reserve reserve) {
			this.reserve = reserve;
		}
	}
	
	@DELETE
	public String deleteReserve(@QueryParam("username") String username,
			@QueryParam("token") String token,@QueryParam("id") Long reserveId) {
		
		Agent agent = agentDao.getByUsername(username);
		boolean error = true;
		
		if (agent != null
				&& agent.getToken() != null && agent.getToken().equals(token)
				&& agent.getTokenTTL() != null
				&& agent.getTokenTTL().compareTo(new Date()) > 0) {
			
			if(agent instanceof Admin){
				reserveDao.delete(reserveId);
				error = false;
			}else if(agent instanceof Teacher) {
				Reserve reserve = reserveDao.findById(reserveId);
				if(reserve != null && reserve.getTeacher() != null && reserve.getTeacher().getId() == agent.getId()){
					reserveDao.delete(reserveId);
					error = false;
				}
			}else if(agent instanceof Secretary) {
				reserveId = reserveDao.getReserve(reserveId, agent.getId());
				if(reserveId != null){
					reserveDao.delete(reserveId);
					error = false;
				}
			}
		}
		if (error) {
			return "Not Allowed!";
		}
		return "Success!";
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveReserve(@QueryParam("username") String username,
			@QueryParam("token") String token, Reserve reserve) {

		ValidatorResponse v = new ValidatorResponse();
		v.setReserve(reserve);
		
		if(fieldsValidator(v) && roomValidtor(v) && isAvailable(v) && agentValidator(v, username, token) && teacherValidator(v)){
			reserve.setRequestedIn(new Date());
			reserve.setReserveGranted(null);
			reserveDao.save(reserve);
			v.setMessage(OK);
		}

		return Response.ok(v.getMessage()).status(Status.OK).build();
	}
	
	
	
	
	@GET
	@Path("/room/{roomId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReserves(@PathParam("roomId") String roomId) {

		List<Reserve> reserves = new ArrayList<Reserve>();
		try{reserves = reserveDao.getByRoomIdToday(Long.parseLong(roomId));}
		catch(Exception e){}

		return Response.ok(reserves).status(Status.OK).build();
	}
	
	@GET
	@Path("/pendent")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOutstandingReservations(@QueryParam("username") String username,
			@QueryParam("token") String token, @QueryParam("agentId") Long agentId) {
		Agent agent = agentDao.findById(agentId);
		List<Reserve> reserves = new ArrayList<Reserve>();
		
		if (agent != null
				&& agent.getToken() != null && agent.getToken().equals(token)
				&& agent.getTokenTTL() != null
				&& agent.getTokenTTL().compareTo(new Date()) > 0) {
		
			if(agent instanceof Teacher){
				reserves = reserveDao.getPendentsForTeachers((Teacher) agent);
			}else {
				reserves = reserveDao.getPendents(agent);
			}
		}
		
		return Response.ok(reserves).status(Status.OK).build();
	}
	
	

	private boolean fieldsValidator(ValidatorResponse v) {
		Reserve reserve = v.getReserve();

		if (reserve != null
				&& reserve.getBeginMillis() != null
				&& reserve.getEndMillis() != null) {
			return true;
		}
		v.setMessage(MISSING_FIELDS);
		return false;
	}

	private boolean isAvailable(ValidatorResponse v) {
		Reserve reservation = v.getReserve();

		if (reserveDao.isAvailable(reservation.getRoom().getId(),
				reservation.getBeginMillis(), reservation.getEndMillis())) {
			return true;
		}
		v.setMessage(NOT_AVAILABLE);
		return false;
	}

	private boolean agentValidator(ValidatorResponse v, String username,
			String token) {
		Agent agent = agentDao.getByUsername(username);
		if (agent != null
				&& (agent instanceof Admin || agent instanceof Secretary || agent instanceof Teacher)
				&& agent.getToken() != null && agent.getToken().equals(token)
				&& agent.getTokenTTL() != null
				&& agent.getTokenTTL().compareTo(new Date()) > 0) {
			v.setRequester(agent);
			return true;
		}
		v.setMessage(UNAUTHORIZED);
		return false;
	}

	private boolean teacherValidator(ValidatorResponse v) {
		Agent agent = v.getRequester();
		Reserve reserve = v.getReserve();
		if (agent != null && reserve != null) {
			if (agent instanceof Admin || agent instanceof Secretary) {
				if (reserve.getTeacher() != null
						&& reserve.getTeacher().getId() != null) {
					Teacher teacher = teacherDao.findById(reserve.getTeacher()
							.getId());
					if (teacher != null) {
						reserve.setReserveGranted(true);
						v.setTeacher(teacher);
						return true;
					} else {
						v.setMessage(NOT_TEACHER);
					}
				} else {
					v.setMessage(MISSING_FIELDS);
				}
			} else if (agent instanceof Teacher) {
				v.setTeacher((Teacher) agent);
				reserve.setReserveGranted(false);
				return true;
			} else {
				v.setMessage(UNAUTHORIZED);
			}
		} else {
			v.setMessage(MISSING_FIELDS);
		}
		return false;
	}
	
	private boolean roomValidtor(ValidatorResponse v) {
		Reserve reserve = v.getReserve();
		if(reserve.getRoom() != null && reserve.getRoom().getId() != null) {
			Room room = roomDao.findById(reserve.getRoom().getId());
			if(room != null) {
				reserve.setRoom(room);
				return true;
			}else{
				v.setMessage(ROOM_DOESNT_EXIST);
			}
		}else{
			v.setMessage(MISSING_FIELDS);
		}
		return false;
	}

}
