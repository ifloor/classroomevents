package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.AgentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.ReserveDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.RoomDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.SecretaryDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.util.WebTools;

@ManagedBean
@SessionScoped
public class LoginMB implements Serializable{
	private static final long serialVersionUID = 7170592509484398741L;
	
	private Boolean logged=false;
	private String userLogin,passwordLogin;
	private Agent agentLogged;
	
	@EJB private AgentDao aDao;
	@EJB private RoomDao rDao;
	@EJB private SecretaryDao sDao;
	@EJB private ReserveDao reDao;
	
	private int numNotifications;
	

	
	public void tryLogin(){
		if(userLogin.equals("admin")){
			Agent a = aDao.getByUsername(userLogin);
			if( a == null){
				a = new Admin();
				a.setFullname("Administrador");
				a.setUsername("admin");
				a.setPassword(Agent.hashPassword("rebinboca"));
				aDao.save(a);
			}
		}
		
		Agent a = aDao.getByUsername(userLogin);
		if(a == null || ! a.getPassword().contentEquals(Agent.hashPassword(passwordLogin))){
			failLoginAttempt();
		}else{
			userLogin = "";
			passwordLogin = "";
			logged = true;
			agentLogged = a;
			updateNumNotf();
			WebTools.gotoPage("/management/home.xhtml");
		}
	}
	
	public void updateNumNotf(){
		numNotifications = reDao.getPendents(agentLogged).size();
	}
	
	public void failLoginAttempt(){
		userLogin = "";
		passwordLogin = "";
		FacesContext ctx = FacesContext.getCurrentInstance();
		ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
				"Usuário e/ou senha inválidos", "Erro no login"));
		logged = false;
	}
	
	public void logout(){
		logged = false;
		agentLogged = null;
		WebTools.gotoPage("/index.xhtml");
	}
	
	
	
	
	public int getNumNotifications() {
		return numNotifications;
	}

	public void setNumNotifications(int numNotifications) {
		this.numNotifications = numNotifications;
	}

	public String getPasswordLogin() {
		return passwordLogin;
	}
	public void setPasswordLogin(String passwordLogin) {
		this.passwordLogin = passwordLogin;
	}
	public Boolean getLogged() {
		return logged;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public Agent getAgentLogged() {
		return agentLogged;
	}

	public void setAgentLogged(Agent agentLogged) {
		this.agentLogged = agentLogged;
	}

	
	
	
	
	
	
	
	
}
