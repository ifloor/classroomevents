package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Floor;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;
import br.ufscar.dc.gsdr.ClassroomEvents.util.Retangulo;


@Stateless
@TransactionManagement
public class DepartmentDao extends HibernateEntityManagerGenericDao<Department, Long>{

	public DepartmentDao() {
		super(Department.class);
	}
	
	@EJB FloorDao fDao;
	@EJB SecretaryDao sDao;
	
	public Department loadDepartmentWithSecretaries(Long departmentId){
		Criteria  crit = getSession().createCriteria(Department.class);
		crit.add(Restrictions.eq("id", departmentId));
		Department dep = (Department)crit.uniqueResult();
		if(dep != null){
			dep.getSecretaries().size();
		}
		return dep;
	} 
	
	@SuppressWarnings("unchecked")
	public List<Department> getDepartments(String id){
		Criteria crit = getSession().createCriteria(Department.class);
		crit.createAlias("institution", "inst");
		crit.add(Restrictions.eq("inst.id", id));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Department> getDepartmentsIn(Retangulo ret){
		Criteria crit = getSession().createCriteria(Department.class);
		
		crit.add(
			Restrictions.disjunction().add(
					Restrictions.conjunction().add(
						Restrictions.ge("xa", ret.getP1().getX())
					).add(
						Restrictions.le("xa", ret.getP2().getX())
					).add(
						Restrictions.ge("ya", ret.getP1().getY())
					).add(
						Restrictions.le("ya", ret.getP2().getY())
					)
			).add(
					Restrictions.conjunction().add(
						Restrictions.ge("xb", ret.getP1().getX())
					).add(
						Restrictions.le("xb", ret.getP2().getX())
					).add(
						Restrictions.ge("yb", ret.getP1().getY())
					).add(
						Restrictions.le("yb", ret.getP2().getY())
					)
			)
		);
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Department> findAllOrderedByName(){
		Criteria crit = getSession().createCriteria(Department.class);
		crit.addOrder(Order.asc("name"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Department> getByAgent(Agent agent){
		//Allow access to departments throught institutes highers
		if(agent instanceof Admin){
			return findAllOrderedByName();
		}
		Criteria crit = getSession().createCriteria(Department.class);
		crit.createAlias("institution", "insti");
		crit.createAlias("insti.secretaries", "secsInt");
		crit.add(Restrictions.eq("secsInt.id", agent.getId()));
		crit.addOrder(Order.asc("name"));
		List<Department> indirect = crit.list();
		
		crit = getSession().createCriteria(Department.class);
		crit.createAlias("secretaries", "secs");
		crit.add(Restrictions.eq("secs.id", agent.getId()));
		List<Department> directly = crit.list();
		Set<Department> s = new TreeSet<Department>();
		s.addAll(directly);
		s.addAll(indirect);
		return ((List<Department>) (List<?>) Arrays.asList(s.toArray()));
	}
	
	/**
	 * Return only the departments diretly accessed
	 * @param Agent
	 * @return List<Department>
	 */
	@SuppressWarnings("unchecked")
	public List<Department> getByAgentStrictly(Agent agent){
		if(agent instanceof Admin){
			return findAllOrderedByName();
		}
		Criteria crit = getSession().createCriteria(Department.class);
		crit.createAlias("secretaries", "secs");
		crit.add(Restrictions.eq("secs.id", agent.getId()));
		crit.addOrder(Order.asc("name"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Department> getDepartments(Long institutionId){
		Criteria crit = getSession().createCriteria(Department.class);
		crit.createAlias("institution", "ins");
		crit.add(Restrictions.eq("ins.id", institutionId));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public void deleteWithFloors(Long departmentId){
		List<Floor> floors = fDao.getFloors(departmentId);
		for (Floor floor : floors) {
			fDao.deleteWithRooms(floor.getId());
		}
		
		List<Secretary> secretaries = null;
		Criteria crit = getSession().createCriteria(Secretary.class);
		crit.createAlias("departments", "d");
		crit.add(Restrictions.eq("d.id", departmentId));
		secretaries = crit.list();
		if(secretaries != null){
			for (Secretary secretary : secretaries) {
				for (int i = 0; i < secretary.getDepartments().size(); i++) {
					secretary.getDepartments().remove(i);
					i--;
				}
				sDao.updateFlushing(secretary);
			}
		}
		Department d = findById(departmentId);
		d.setSecretaries( null);
		update(d);
		getSession().flush();
		delete(departmentId);
	}
}
