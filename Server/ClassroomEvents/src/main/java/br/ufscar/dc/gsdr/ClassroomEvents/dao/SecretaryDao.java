package br.ufscar.dc.gsdr.ClassroomEvents.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;

@Stateless
@TransactionManagement
public class SecretaryDao extends HibernateEntityManagerGenericDao<Secretary, Long>{

	public SecretaryDao(){
		super(Secretary.class);
	}
	
	public Secretary loadSecretaryWithAllLazyLists(Long secretaryId){
		Criteria crit = getSession().createCriteria(Secretary.class);
		crit.add(Restrictions.eq("id", secretaryId));
		Secretary s = (Secretary)crit.uniqueResult();
		if(s != null){
			s.getInstitutions().size();
			s.getDepartments().size();
			s.getFloors().size();
			s.getRooms().size();
		}
		return s;
	}
	
	@SuppressWarnings("unchecked")
	public List<Secretary> findAllOrderedByName(){
		Criteria crit = getSession().createCriteria(Secretary.class);
		crit.addOrder(Order.asc("username"));
		return crit.list();
	}
	
	public Secretary findByUsername(String username){
		Criteria crit = getSession().createCriteria(Secretary.class);
		crit.add(Restrictions.eq("username", username));
		Object obj = crit.uniqueResult();
		if(obj != null){
			return (Secretary)obj;
		}else{
			return null;
		}
	}
	public Secretary findByUsernameWithRooms(String username){
		Criteria crit = getSession().createCriteria(Secretary.class);
		crit.add(Restrictions.eq("username", username));
		Object obj = crit.uniqueResult();
		if(obj != null){
			((Secretary)obj).getRooms().size();
			return (Secretary)obj;
		}else{
			return null;
		}
	}
	
	public void updateFlushing(Secretary sec){
		update(sec);
		getSession().flush();
	}
	
	
}
