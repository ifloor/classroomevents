package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.AgentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.enumeration.Entities;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.util.WebTools;

@ManagedBean
@ViewScoped
public class UserCreateMB implements Serializable{
	private static final long serialVersionUID = 78683333876250458L;
	
	@EJB private AgentDao agDao;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	private Agent agentLogged,agent;
	private Entities entityChoosen = Entities.ADMIN;
	
	private Entities[] entitiesTypes = Entities.values();
	
	private String password, confirmPassword;
	
	
	public UserCreateMB(){
		
	}
	
	@PostConstruct
	public void init(){
		agentLogged = loginMB.getAgentLogged();
		instantiateNewAgent();
	}

	public void instantiateNewAgent(){
		Agent oldAgent = agent;
		agent = Agent.instantiateNew(entityChoosen);
		if(oldAgent != null){
			agent.setFullname(oldAgent.getFullname());
			agent.setPassword(oldAgent.getPassword());
			agent.setUsername(oldAgent.getUsername());
		}
	}
	
	public void save(){
		if(agent != null){
			Agent ag = agDao.getByUsername(agent.getUsername());
			if(ag == null){
				if(password.equals(confirmPassword)){
					agent.setPassword(Agent.hashPassword(password));
					agDao.save(agent);
					WebTools.gotoPage("/management/user/list.xhml");
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuário criado com sucesso", "Sucesso!");
					FacesContext.getCurrentInstance().addMessage(null, fm);
				}else{
				//senhas não conferem
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "As senhas não são iguais", "Erro");
					FacesContext.getCurrentInstance().addMessage(null, fm);
				}
			}else{
				//usuário ja existe
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário já existe", "Erro");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			}
		}
	}
	
	
	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public Agent getAgentLogged() {
		return agentLogged;
	}

	public void setAgentLogged(Agent agentLogged) {
		this.agentLogged = agentLogged;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Entities getEntityChoosen() {
		return entityChoosen;
	}

	public void setEntityChoosen(Entities entityChoosen) {
		this.entityChoosen = entityChoosen;
	}

	public Entities[] getEntitiesTypes() {
		return entitiesTypes;
	}

	public void setEntitiesTypes(Entities[] entitiesTypes) {
		this.entitiesTypes = entitiesTypes;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	
	
	
	
	

}
