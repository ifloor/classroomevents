package br.ufscar.dc.gsdr.ClassroomEvents.images;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import br.ufscar.dc.gsdr.ClassroomEvents.enumeration.Building;

@Stateful
public class ImageDaemon {
	protected String imagesPath;
	public static final String DEFAULT_FILENAME="default.png";
	
	
	@PostConstruct
	public void init(){
		buildPathForImages();
		//notar que no caso da busca, não tem o que fazer, então convem que venha null no contexto
		if(imagesPath != null && imagesPath.length() > 0){
			createImagesFolderIfNotExists();
		}
	}
	
	public ImageDaemon(){
		
	}
	
	
	public File getImageFile(Building entity, Long id, String specificFileName){
		File f = new File(imagesPath+File.separator+entity.toString()+File.separator+id+File.separator+specificFileName);
		if(f.exists()){
			return f;
		}else
			return null;
	}
	
	public File getImageFile(Building entity, Long id){
		return getImageFile(entity, id, DEFAULT_FILENAME);
	}
	
	public StreamedContent getImage(Building entity, Long id, String specificFileName){
		File f = new File(imagesPath+File.separator+entity.toString()+File.separator+id);
		if(f.exists()){
			String extension = "";
			extension = FilenameUtils.getExtension(f.getName());
			DefaultStreamedContent streamContent = null;
			FileInputStream finStream = null;
			try {
				finStream = new FileInputStream(f);
				streamContent = new DefaultStreamedContent(finStream, "image/"+extension);
				finStream.close();
				return streamContent;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}else
			return null;
	}
	
	public StreamedContent getImage(Building entity, Long id){
		return getImage(entity, id, DEFAULT_FILENAME);
	}
	
	public void saveImage(UploadedFile upFile, Building entity,Long id, String specificFileName){
		FileImageOutputStream imgOut = null;
		File f = null;
		try {
			createIfNotExists(imagesPath+File.separator+entity.toString()+File.separator+id);
			f = new File(imagesPath+File.separator+entity.toString()+File.separator+id+File.separator+specificFileName);
			imgOut = new FileImageOutputStream(f);
			imgOut.write(upFile.getContents(), 0, upFile.getContents().length);
			imgOut.close();
		} catch (FileNotFoundException e) {} 
		catch (IOException e) {}
	}
	
	public void saveImage(UploadedFile upFile, Building entity, Long id){
		saveImage(upFile, entity, id, DEFAULT_FILENAME);
	}
	
	
	
	protected void buildPathForImages(){
		if(FacesContext.getCurrentInstance() != null){
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String path = servletContext.getRealPath("");
			imagesPath = path + "." +  "images" + File.separator;
		}
	}
	
	public void injectPathForImages(String pathForImages){
		this.imagesPath = pathForImages;
	}
	
	
	protected void createImagesFolderIfNotExists(){
		File path = new File(imagesPath);
		if(!path.exists())
			path.mkdir();
	}
	
	protected void createFloorFolderIfNotExists(Long floorId){
		File path = new File(imagesPath+File.separator+floorId.toString()+File.separator);
		if(! path.exists())
			path.mkdir();
	}
	
	protected void createIfNotExists(String path){
		File f = new File(path);
		if(!f.exists())
			f.mkdirs();
	}
}
