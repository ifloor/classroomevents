package br.ufscar.dc.gsdr.ClassroomEvents.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.TeacherDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;

@Path("/user/student")
public class StudentWebService {

	@EJB private TeacherDao tDao;
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Secretary get(@PathParam("id") String id){
		Secretary stu = new Secretary();
		stu.setFullname("Student fudencio");
		stu.setPassword("pass:");
		stu.setUsername("Naminho");
		
		Room r = new Room();
		r.setName("Sala dos psicos");
		r.setXa(1000d);
		r.setXb(1002d);
		r.setYa(1004d);
		r.setYb(1006d);
		List<Secretary> secs = new ArrayList<Secretary>();
		secs.add(stu);
		r.setSecretaries(secs);
		
		List<Room> rooms = new ArrayList<Room>();
 		rooms.add(r);
		stu.setRooms(rooms);
		
//		Teacher t = new Teacher();
//		t.setFullname("nomusco");
//		t.setPassword("senha do manolo");
//		t.setUsername("Teacher jhow");
//		tDao.save(t);
		
		//return stu;
		return stu;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String body){
		
		return Response.ok(""+body+"\n!").build();
	}
	
	
	
}
