package br.ufscar.dc.gsdr.ClassroomEvents.rest;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FilenameUtils;

import br.ufscar.dc.gsdr.ClassroomEvents.enumeration.Building;
import br.ufscar.dc.gsdr.ClassroomEvents.images.ImageDaemon;

@Path("/images")
public class ImageService implements Serializable{
	private static final long serialVersionUID = 2652923515710318097L;

	@Context ServletContext servletContext;
	
	@EJB private ImageDaemon imgService;
	
	
	@GET
	@Produces("image/png")
	@Path("/{entity}/{entityId}")
	public Response getImage(@PathParam("entity") String entity, @PathParam("entityId") String entityId, @QueryParam("specificName") String specificName){
		try{
			int index = entityId.lastIndexOf(".");
			if(index > 0){
				entityId = entityId.substring(0,index);
			}
			if(entity != null && entity.length() > 0 && Building.valueOf(entity.toUpperCase()) != null){
				String path = servletContext.getRealPath("");
				String imagesPath = path + "." +  "images" + File.separator;
				imgService.injectPathForImages(imagesPath);
				File file =  imgService.getImageFile(Building.valueOf(entity.toUpperCase()), Long.parseLong(entityId));
				if(file != null){
					String extension = FilenameUtils.getExtension(file.getName());
					try {
						BufferedImage buffImg = ImageIO.read(file);
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						ImageIO.write(buffImg, extension, baos);
						byte[] imgBytes = baos.toByteArray();
						if(! new File(servletContext.getRealPath("") + File.separator + "fotos").exists()){
							File fil = new File(servletContext.getRealPath("") + File.separator + "fotos");
							fil.mkdirs();
						}
				        String newFileName = servletContext.getRealPath("") + File.separator + "fotos" + File.separator + "raw" + ".png";
				    	FileImageOutputStream imageOutput;
				        try {  
				        	File f = new File(newFileName);
				            imageOutput = new FileImageOutputStream(f);  
				            imageOutput.write(imgBytes, 0, imgBytes.length);  
				            imageOutput.close();  
				        }  
				        catch(Exception e) {
				        	e.printStackTrace();
				            //throw new FacesException("Erro ao gravar foto.");  
				        }
						
						return Response.ok(new ByteArrayInputStream(imgBytes)).build();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}else{
					return Response.ok("Image not found").build();
				}
			}
			return Response.ok("Image not found").build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.ok("Image not found").build();	
		}
	}
}
