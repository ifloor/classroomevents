package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.ReserveDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.RoomDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.TeacherDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Reserve;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Teacher;

@ManagedBean
@ViewScoped
public class ReserveCreateMB implements Serializable{
	private static final long serialVersionUID = -7970864871380274963L;
	
	@EJB private TeacherDao tDao;
	@EJB private RoomDao roDao;
	@EJB private ReserveDao reDao;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	private Agent agent;
	
	private Reserve reserve;
	private Date begin,end;
	
	private Boolean specificTeacher = true;
	
	private List<Teacher> teachers;
	private List<Room> rooms;
	
	@PostConstruct
	public void init(){
		agent = loginMB.getAgentLogged();
		reserve = new Reserve();
		teachers = tDao.findAllOrderedByName();
		rooms = roDao.getByAgent(agent);
	}
	
	public List<Teacher> completeTeacher(String query) {  
        List<Teacher> suggestions = new ArrayList<Teacher>();  
          
        if(teachers != null){
	        for(Teacher t : teachers) {  
	            if(t.getFullname().toLowerCase().contains(query.toLowerCase()) || t.getUsername().toLowerCase().contains(query.toLowerCase()))  
	                suggestions.add(t);  
	        }
        }
          
        return suggestions;  
    }
	
	public List<Room> completeRoom(String query) {  
        List<Room> suggestions = new ArrayList<Room>();
        String[] queries = query.split(Pattern.quote(" "));
          
       if(rooms != null && queries != null && queries.length > 0){
	        for(Room r : rooms) {  
	        	for (int i = 0; i < queries.length; i++) {
	        		if(r.getName().toLowerCase().contains(queries[i].toLowerCase()) || r.getFloor().getName().toLowerCase().contains(queries[i].toLowerCase())
	        			|| r.getFloor().getDepartment().getName().toLowerCase().contains(queries[i].toLowerCase()) 
	        			|| r.getFloor().getDepartment().getInstitution().getName().toLowerCase().contains(queries[i].toLowerCase()))  
		                suggestions.add(r);
				}     
	        }
       }
          
        return suggestions;  
    }
	
	public String save(){
		//Requested in
		//Granted
		if(reserve != null){
			if(!checkDatesProblem()){
				if(roDao.isAvailable(reserve.getRoom().getId(), begin.getTime(), end.getTime())){
					reserve.setBeginMillis(begin.getTime());
					reserve.setEndMillis(end.getTime());
					reserve.setUserReserved(agent.getId());
					reserve.setRequestedIn(new Date());
					reserve.setReserveGranted(true);
					reDao.save(reserve);
					return "reserveList";
				}else{
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Horário indisponível", "Erro");
					FacesContext.getCurrentInstance().addMessage(null, fm);
					return "";
				}
			}else{
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Problema nas datas: Data fim menor que data começo", "Erro");
				FacesContext.getCurrentInstance().addMessage(null, fm);
				return "";
			}
		}else{
			return "";
		}
	}
	
	public void defineChooseSpecific(){
		if(specificTeacher){
			reserve.setTeacherName("");
		}else{
			reserve.setTeacher(null);
		}
	}
	
	public boolean checkDatesProblem(){
		if(begin.compareTo(end) >= 0){
			begin = ((Date)end.clone());
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(begin);
			cal.add(Calendar.SECOND, -1);
			begin = cal.getTime();
			return true;
		}
		return false;
	}

	public Reserve getReserve() {
		return reserve;
	}

	public void setReserve(Reserve reserve) {
		this.reserve = reserve;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Boolean getSpecificTeacher() {
		return specificTeacher;
	}

	public void setSpecificTeacher(Boolean specificTeacher) {
		this.specificTeacher = specificTeacher;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public List<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}
	
	
	
	

}
