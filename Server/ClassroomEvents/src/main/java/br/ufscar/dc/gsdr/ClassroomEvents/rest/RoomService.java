package br.ufscar.dc.gsdr.ClassroomEvents.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.FloorDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.RoomDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Room;

@Path("/room")
public class RoomService {
	
	@EJB
	RoomDao roomDao;
	
	@EJB
	FloorDao floorDao;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	public Response getRoom(String id){
		try{
			// TODO debugar o pq da pasta não estar criando
			List<Room> rooms = roomDao.getFloors(Long.parseLong(id));
			
			return Response.ok(rooms).status(Status.OK).build();
		}catch(Exception e){
			return Response.ok(e.getMessage()).status(Status.OK).build();
		}
	}
	
}
