package br.ufscar.dc.gsdr.ClassroomEvents.rest;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.ufscar.dc.gsdr.ClassroomEvents.Service.Token;
import br.ufscar.dc.gsdr.ClassroomEvents.Service.TokenService;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.AgentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Admin;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Secretary;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Student;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Teacher;

 

@Path("/user/login")
public class LoginWebService {

	private TokenService tokenService = TokenService.getInstance();

	@EJB private AgentDao agDao;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response tryLogin(@QueryParam("username") String username,
		@QueryParam("password") String passwod){
		
		Agent agent = agDao.getByUsername(username);
		
		if(agent != null){
			if(agent.getPassword().equals(Agent.hashPassword(passwod))){
				String entity="";
				if(agent instanceof Admin){
					entity = "admin";
				}else{
					if(agent instanceof Student){
						entity = "student";
					}else{
						if(agent instanceof Teacher){
							entity = "teacher";
						}else{
							if(agent instanceof Secretary){
								entity = "secretary";
							}
						}
					}
				}
				Token t =  tokenService.createNewToken(entity, username);
				agent.setToken(t.getValue());
				agent.setTokenTTL(t.getTTL());
				
				t.setFullName(agent.getFullname());
				agDao.update(agent);
				
				return Response.ok(t).build();
			}else{
				return Response.ok("Combination username/password is invalid").status(Status.FORBIDDEN).build();
			}
		}else{
			return Response.ok("username not known").status(Status.BAD_REQUEST).build();
		}
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkLogin(@QueryParam("username") String username,
			@QueryParam("token") String token){
//		Admin admin = new Admin();
//		admin.setFullname("Igor Maldonado Floôr");
//		admin.setUsername("igor");
//		admin.setPassword(Admin.hashPassword("igor"));
//		aDao.save(admin);
		
		Agent a = agDao.getByUsername(username);
		if(a instanceof Admin)
			System.out.println("is admin");
		if(a instanceof Secretary)
			System.out.println("secretary");
		return Response.ok(a).status(Status.OK).build();
		
//		if(allowedEntity(entity)){
//			if(tokenService.hasToken(entity, username, token)){
//				return Response.ok("Okay!").build();
//			}else{
//				return Response.ok("Combination token/username is invalid").status(Status.FORBIDDEN).build();
//			}
//		}else{
//		
//			return Response.ok("Entity not known").status(Status.NOT_FOUND).build();
//		}
	} 
}
