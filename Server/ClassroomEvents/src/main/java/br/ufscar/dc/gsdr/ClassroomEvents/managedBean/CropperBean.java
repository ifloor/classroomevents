package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.CroppedImage;

@ManagedBean
@ViewScoped
public class CropperBean implements Serializable{
	private static final long serialVersionUID = -5077938496138125513L;
	
	private CroppedImage imageCropped;
	
	public CropperBean(){
		
	}
	
	@PostConstruct
	public void init(){
		
	}

	public CroppedImage getImageCropped() {
		return imageCropped;
	}

	public void setImageCropped(CroppedImage imageCropped) {
		this.imageCropped = imageCropped;
	}
	
	

}
