package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.DepartmentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.dao.InstitutionDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Institution;
import br.ufscar.dc.gsdr.ClassroomEvents.util.WebTools;

@ManagedBean
@ViewScoped
public class DepartmentCreateMB implements Serializable{
	private static final long serialVersionUID = 5733680385801530200L;
	
	@EJB private InstitutionDao iDao;
	@EJB private DepartmentDao dDao;
	
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	@ManagedProperty(value="#{mapBean}")
	private MapBean mapBean;
	
	private Agent agent;
	
	private List<Institution> institutions;
	
	private Department department;
	private Institution institution;
	
	@PostConstruct
	public void init(){
		agent = loginMB.getAgentLogged();
		if(! agent.isAdmin())
			WebTools.gotoPage("/management/institution/list.xhtml");
		department = new Department();
		institutions = iDao.findAllOrderedByName();
	}
	
	public List<Institution> completeInstitution(String query) {  
        List<Institution> suggestions = new ArrayList<Institution>();  
          
        for(Institution inst : getInstitutions()) {  
            if(inst.getName().toLowerCase().contains(query.toLowerCase()))  
                suggestions.add(inst);  
        }  
          
        return suggestions;  
    }  
	
	public String save(){
		department.setXa(mapBean.getXa());
		department.setXb(mapBean.getXb());
		department.setYa(mapBean.getYa());
		department.setYb(mapBean.getYb());
		department.setInstitution(institution);
		dDao.save(department);
		return "departmentList";
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
//		System.out.println("institution selected: "+institution.getName());
		this.institution = institution;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public MapBean getMapBean() {
		return mapBean;
	}

	public void setMapBean(MapBean mapBean) {
		this.mapBean = mapBean;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<Institution> getInstitutions() {
		if(institutions == null)
			institutions = iDao.findAllOrderedByName();
		return institutions;
	}

	public void setInstitutions(List<Institution> institutions) {
		this.institutions = institutions;
	}
	
	

	
	

	
	
}
