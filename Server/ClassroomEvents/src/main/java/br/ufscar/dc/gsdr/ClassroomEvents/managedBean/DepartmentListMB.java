package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.DepartmentDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Department;

@ManagedBean
@ViewScoped
public class DepartmentListMB implements Serializable{
	private static final long serialVersionUID = 5733680385801530200L;
	
	@EJB private DepartmentDao dDao;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	private Agent agent;
	
	private List<Department> departments,filteredDepartments;

	private Department department;
	
	@PostConstruct
	public void init(){
		agent = loginMB.getAgentLogged();
		departments = dDao.getByAgent(agent);
	}

	public void deleteDepartment(Department d){
		dDao.deleteWithFloors(d.getId());
		for (int i = 0; i < departments.size(); i++) {
			if(d.getId().equals(departments.get(i).getId())){
				departments.remove(i);
			}
		}
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Departamento deletado com sucesso", "Sucesso!");
		FacesContext.getCurrentInstance().addMessage(null, fm);
	}
	

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<Department> getFilteredDepartments() {
		return filteredDepartments;
	}

	public void setFilteredDepartments(List<Department> filteredDepartments) {
		this.filteredDepartments = filteredDepartments;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	
	
	
	
	
	
	
}
