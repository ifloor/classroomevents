package br.ufscar.dc.gsdr.ClassroomEvents.managedBean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.ufscar.dc.gsdr.ClassroomEvents.dao.ReserveDao;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Agent;
import br.ufscar.dc.gsdr.ClassroomEvents.model.Reserve;
import br.ufscar.dc.gsdr.ClassroomEvents.util.WebTools;

@ManagedBean
@ViewScoped
public class ReserveListMB implements Serializable{
	private static final long serialVersionUID = -7970864871380274963L;
	
	@EJB private ReserveDao rDao; 
	private List<Reserve> reservesPendent, reservesHistorical;
	
	private Reserve reserve;
	
	private Boolean historicalGranted = true;
	
	@ManagedProperty(value="#{loginMB}")
	private LoginMB loginMB;
	
	private Agent agentLogged;
	
	@PostConstruct
	public void init(){
		agentLogged= loginMB.getAgentLogged();
		if(agentLogged == null){
			WebTools.gotoHomeSystem();
		}else{
			if(agentLogged.isAdmin() || agentLogged.isSecretary())
				buildAdminOrSecretary();
			else
				if(agentLogged.isTeacher())
					buildTeacher();
		}
	}
	
	public void grantReservePendent(Reserve r){
		if(rDao.isAvailable(r.getRoom().getId(), r.getBeginMillis(), r.getEndMillis())){
			r.setReserveGranted(true);
			rDao.update(r);
			reservesPendent.remove(r);
			reservesHistorical.add(0, r);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Reserva concedida com sucesso", "SUCESSO" );
			FacesContext.getCurrentInstance().addMessage(null, fm);
			loginMB.updateNumNotf();
		}else{
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Desculpe já existe uma reserva neste horário", "ERRO" );
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}
	}
	
	public void prepareReservePendent(Reserve r){
		this.reserve = r;
	}
	
	public void denyReservePendent(Reserve r){
		r.setReserveGranted(false);
		rDao.update(r);
		reservesPendent.remove(r);
		reservesHistorical.add(0, r);
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Reserva negada com sucesso", "SUCESSO" );
		FacesContext.getCurrentInstance().addMessage(null, fm);
		loginMB.updateNumNotf();
	}
	
	
	public void grantReserveHistorical(Reserve r){
		if(rDao.isAvailable(r.getRoom().getId(), r.getBeginMillis(), r.getEndMillis())){
			r.setReserveGranted(true);
			rDao.update(r);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Reserva concedida com sucesso", "SUCESSO" );
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}else{
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Desculpe já existe uma reserva neste horário", "ERRO" );
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}
	}
	
	public void prepareReserveHistorical(Reserve r){
		prepareReservePendent(r);
	}
	
	public void denyReserveHistorical(Reserve r){
		r.setReserveGranted(false);
		rDao.update(r);
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Reserva negada com sucesso", "SUCESSO" );
		FacesContext.getCurrentInstance().addMessage(null, fm);
	}
	
	public void markPendent(Reserve r){
		r.setReserveGranted(null);
		rDao.update(r);
		reservesHistorical.remove(r);
		reservesPendent.add(0, r);
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Reserva marcada como não decidida com sucesso", "SUCESSO" );
		FacesContext.getCurrentInstance().addMessage(null, fm);
		loginMB.updateNumNotf();
	}
	
	private void buildAdminOrSecretary(){
		reservesPendent = rDao.getPendents(agentLogged);
		reservesHistorical = rDao.getHistorical(agentLogged);
	}
	
	private void buildTeacher(){
		//TODO
	}
	
	

	public Boolean getHistoricalGranted() {
		return historicalGranted;
	}

	public void setHistoricalGranted(Boolean historicalGranted) {
		this.historicalGranted = historicalGranted;
	}

	public List<Reserve> getReservesPendent() {
		return reservesPendent;
	}

	public void setReservesPendent(List<Reserve> reservesPendent) {
		this.reservesPendent = reservesPendent;
	}

	public List<Reserve> getReservesHistorical() {
		return reservesHistorical;
	}
	
	public void setReservesHistorical(List<Reserve> reservesHistorical) {
		this.reservesHistorical = reservesHistorical;
	}

	public LoginMB getLoginMB() {
		return loginMB;
	}

	public void setLoginMB(LoginMB loginMB) {
		this.loginMB = loginMB;
	}

	public Reserve getReserve() {
		return reserve;
	}

	public void setReserve(Reserve reserve) {
		this.reserve = reserve;
	}
	
	

	
}
