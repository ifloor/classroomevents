$ = jQuery;

var countRooms = 0;

function addBlackPanelOnSubmit(){
	var left = parseInt($(".jcrop-holder")[0].children[0].style.left);
	var top = parseInt($(".jcrop-holder")[0].children[0].style.top);
	var width = parseInt($(".jcrop-holder")[0].children[0].style.width);
	var height = parseInt($(".jcrop-holder")[0].children[0].style.height);
	
	addBlackPanel(countRooms++, left, top, width, height, $(PF("txtRoomName").jqId).val());
}

function addBlackPanel(id, left, top, width, height, name){
	id = "room" + id;
	var divAppend = "<div id='" + id + "' class='roomSquare' style='z-index: 300; width: " + width + "px; height: " + height + "px; top: " + top + "px; left: " + left + "px;'><p>" + name + "</p></div>";
	$(".jcrop-holder").append(divAppend);
	
	id = "#" + id;
	var element = $(id)[0];
	var marginTop =  (element.offsetHeight - element.children[0].offsetHeight)/2 + "px";
	var busca = id + " p";
	$(busca).css("margin-top", marginTop);
	
}